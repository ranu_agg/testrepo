const appError = require(`./error.model`);
import { Logger } from '../logger';
const logger = Logger.getInstance(module.filename);
import * as  http_status from 'http-status';

module.exports = function (error, req, res, next) {

    const logParams = {
        url: req.originalUrl,
        method: req.method,
        message:error.message,
        statusCode: http_status.OK,
        stack: []
    };

    if (error instanceof appError.UnAuthorizedAccessError) {
        logParams.statusCode = http_status.UNAUTHORIZED;
        res.status(http_status.UNAUTHORIZED).json({errorCode: error.customErrorCode, message: error.message });
    } else if (error instanceof appError.BadRequestError) {
        logParams.statusCode = http_status.BAD_REQUEST;
        res.status(http_status.BAD_REQUEST).json({errorCode: error.customErrorCode, message: error.message });
    } else if (error instanceof appError.NotFoundError) {
        logParams.statusCode = http_status.NOT_FOUND;
        res.status(http_status.NOT_FOUND).json({errorCode: error.customErrorCode, message: error.message });
    } else if (error instanceof appError.ForbiddenError) {
        logParams.statusCode = http_status.FORBIDDEN;
        res.status(http_status.FORBIDDEN).json({errorCode: error.customErrorCode, message: error.message});
    } else if(error instanceof appError.CustomEngageError){
        logParams.statusCode = http_status.INTERNAL_SERVER_ERROR;
        res.status(http_status.INTERNAL_SERVER_ERROR).json({errorCode: error.customErrorCode, message: error.message});
    } else {
        // Add call stack to error in case of server error
        logParams.statusCode = http_status.INTERNAL_SERVER_ERROR;
        res.status(http_status.INTERNAL_SERVER_ERROR).json({message: error.message });
    }

    logger.error(logParams);

    next();
};