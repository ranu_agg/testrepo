'use strict';

const defaultErrorMessage: string = 'Something went wrong!';
const authorizationErrorMessage: string = 'Invalid or missing access token!';
const serverErrorMessage: string = 'SERVER_ERROR": "Oops! Something is broken';
const requestInvalidMessage: string = 'Request body is invalid';
const notFoundMessage: string = 'Not found';
const forbiddenMessage: string = 'Access Denied';

/**
 * Custom Error class for invalid request errors
 *
 * @class AppError
 * @extends {Error}
 */

class AppError extends Error {
    customErrorCode: string | undefined;
    constructor(message: string, customErrorCode?: string) {
        super(`${message || defaultErrorMessage}`);
        if(customErrorCode) {
            this.customErrorCode = customErrorCode;
        }
    }

}

/**
 * Custom Error class for invalid request errors
 *
 * @class BadRequestError
 * @extends {Error}
 */

export class BadRequestError extends AppError {

    /**
     * Creates an instance of BadRequestError.
     * @param {string} message Error message
     *
     */
    constructor(message: string = requestInvalidMessage) {
        super(message);
    }
}


/**
 * Custom Error class for invalid request errors
 *
 * @class InternalServerError
 * @extends {Error}
 */
export class InternalServerError extends AppError {

    /**
     * Creates an instance of BadRequestError.
     * @param {string} message Error message
     *
     */
    constructor(message: string = serverErrorMessage) {
        super(message);
    }
}

/**
 * Custom Error class for unauthorized request errors
 *
 * @class UnAuthorizedAccessError
 * @extends {Error}
 */
export class UnAuthorizedAccessError extends AppError {

    /**
     * Creates an instance of BadRequestError.
     * @param {string} message Error message
     *
     */
    constructor(message: string = authorizationErrorMessage, customErrorCode?: string) {
        super(message, customErrorCode);
    }
}

/**
 * Custom Error class for missing errors
 *
 * @class NotFoundError
 * @extends {Error}
 */
export class NotFoundError extends AppError {

    /**
     * Creates an instance of BadRequestError.
     * @param {string} message Error message
     *
     */
    constructor(message: string = notFoundMessage, customErrorCode?: string) {
        super(message, customErrorCode);
    }
}

/**
 * Custom ForbiddenError class for missing errors
 *
 * @class ForbiddenError
 * @extends {AppError}
 */
export class ForbiddenError extends AppError {

    /**
     * Creates an instance of ForbiddenError.
     * @param {string} message Error message
     *
     */
    constructor(message: string = forbiddenMessage, customErrorCode?: string) {
        super(message, customErrorCode);
    }
}


export class CustomEngageError extends AppError{

    
    /**
     * Creates an instance of ForbiddenError.
     * @param {string} message Error message
     * @param {string} errorCode Error Code
     *
     */
    constructor(engageError) {
        super(engageError.message, engageError.code);
    }
}


export const EngageErrors =  {
    INVALID_ACCESS_CODE : {
        code : "E001",
        message : "invalid Access code"
    },
    NO_PRODUCT_ASSOCIATED : {
        code : "E002",
        message : "No product associated with given code"
    },
    ACCOUNT_PROVISION_FAILED :{
        code : "E003",
        message : "Error while creating Student account"
    },
    CLASS_ENROLLMENT_FALIED : {
        code : "E004",
        message : "Error while enrolling student in class"
    },
    PROD_ENTITLEMENT_FALIED : {
        code :"E005",
        message : "Error while product entitlement"
    },
    EMAIL_SENDING_FALIED :{
        code :"E006"
    }
}