'use strict';

/* Set process environment variables */
import * as dotenv from 'dotenv';
dotenv.config();

/* dependencies */
import * as express from "express";
import * as bodyParser from 'body-parser';
import config from './config';
import { v1 as uuid } from 'uuid';
const uuId: string = uuid();
//import { createNamespace } from 'continuation-local-storage';
import { Logger } from './logger';
import * as passport from 'passport';
// import * as glob from 'glob';
import * as serverless from 'serverless-http';
import { ConfigManager } from './common/configManager';



// const namespace = createNamespace(config.logger.request.local_storage_key);
const logger = Logger.getInstance(module.filename);

let app = express();

// const expressSwagger = require('express-swagger-generator')(app);
// let options = {
//   swaggerDefinition: {
//       info: {
//           description: 'The core elements of DLS Engage backend is to help DLS Engage UI to communicate with various APIs',
//           title: 'DLS Engage API',
//           version: '1.0.0',
//       },
//       produces: [
//           "application/json",
//           "application/xml"
//       ],
//       schemes: ['http', 'https'],
//       securityDefinitions: {
//           JWT: {
//               type: 'apiKey',
//               in: 'header',
//               name: 'Authorization',
//               description: "",
//           }
//       }
//   },
//   basedir: __dirname, //app absolute path
//   files: ['./routes/**/*.js'] //Path to the API handle folder
// };
// expressSwagger(options);

// Setting correlation id in continuation-local-storage
// app.use(function (req, res, next) {
//   namespace.run(function () {
//     namespace.set(config.logger.request.id, uuId);
//     next();
//   });
// });

let whiteListDomain = config.cors.whiteListDomain.split(',');
//Handling of option method
app.options("/*", function (req, res, next) {
  let origin = req.headers.origin; 
  if(whiteListDomain.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, cache-control,Authorization, Content-Length, X-Requested-With,Origin, X-Requested-With, Content-Type, Accept');
  res.send(200);
});

app.set('etag', false);
//app.use(compression());

// app.use(express.static(path.join(__dirname, '../src/public/')));
app.use(bodyParser.text({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
// app.use(expressValidator());

// CookieParser should be above session
// app.use(cookieParser());

app.use(function (req, res, next) {
  let origin = req.headers.origin;
  if(whiteListDomain.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Headers', 'Content-Type, cache-control,Authorization, Content-Length,Origin, X-Requested-With, Content-Type, Accept');
  return next();
});
//Adding the passport file dls-strategy.server.service in auth.middleware.ts
// glob.sync('./dist/passport/*.js').forEach(function (file) {
//   console.log("app start passport")
//   require(path.resolve(file))(passport);

// });


logger.info('Completing passport configuration');
// Initialize passport
app.use(passport.initialize());



import routeModule from './routes/app.server.routes';
app.use('/', routeModule());

app.get('/', (req, res) => {
  res.send("DLS Integration Api");
});

// error handling
app.use(require('./error/error.handler.service'));

// -----------Start listening -------------------
// app.listen(config.port, function () {
//   logger.info('Your Leonardo Paint application is now running.');
// });

const handler = serverless(app);
module.exports.handler = async (event, context) => {

  //Get Alias Name
  let aliasName = "";
  let apiRequestId = event.requestContext.requestId;
  let lambdaRequestId = context.awsRequestId;
  console.log("API Gateway Request ID: " + apiRequestId + " Lambda Request ID: " + lambdaRequestId);
  config.logger.request.id = apiRequestId;
  try{
    if(context && context.invokedFunctionArn)
    {
        let functionArnArr = context.invokedFunctionArn.split(':');
        
        if(functionArnArr.length > 0 && functionArnArr.length === 8)
        {
          aliasName = functionArnArr[7];

          //Hard coding account id for now
          let accountId = config.account.id;
                  
          //Load alias and client specific data
          await ConfigManager.loadAliasConfig(aliasName, accountId);
        
          //Update config file with client specific values
          await ConfigManager.updateConfigWithClientData(aliasName, accountId)
        }
      }
  }
  catch(e)
  {
    //do nothing
  }
  return await handler(event, context);
};

