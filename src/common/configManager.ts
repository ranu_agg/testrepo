import config from '../config';
import { S3InstanceService } from '../services/aws-s3-instance.service';


export class ConfigManager {

    static aliasConfig: {[k: string]: any} = {};

    static getAliasConfig(): any {
        return this.aliasConfig;
    }

    static setAliasConfig(json): any {
        this.aliasConfig = json;
    }

    static loadAliasConfig = async (aliasName, accountId) => {
        const _aliasConfig = ConfigManager.getAliasConfig();
        if (!_aliasConfig[aliasName]) {
          
          let bucketParams: {[k: string]: any} = {};
          let Alias_Config: {[k: string]: any} = {};
      
          //bucket name
          let bucketName = aliasName != ""? config.aws.s3Bucket + "-" + aliasName : config.aws.s3Bucket;
          
          //Bucket param
          bucketParams = {Bucket: bucketName};
          let objList;
          try
          {
            //list all the objects
            objList = await S3InstanceService.ListObjects(bucketParams);
            config.aws.s3Bucket = bucketName;
          }
          catch(e)
          {
            //For dev alias
            bucketParams = {Bucket: config.aws.s3Bucket};
            objList = await S3InstanceService.ListObjects(bucketParams);
          }
          
          //Read all the client specifc config jsons
          for(let i=0;i<objList.Contents.length;i++)
          {
            let keylist = objList.Contents[i].Key.split('/');
            if(keylist.length === 2 && keylist[1] != "")
            {
              bucketParams.Key = objList.Contents[i].Key;
              let s3Object = await S3InstanceService.GetObject(bucketParams);
              Alias_Config[keylist[0]] = JSON.parse(s3Object.Body.toString());
            }
          }
          console.log("Alias_Config",Alias_Config);
          _aliasConfig[aliasName] = Alias_Config
          
        }
        ConfigManager.setAliasConfig(_aliasConfig);
        
      }

    static updateConfigWithClientData(aliasName, accountId): any {
      const _aliasConfig = ConfigManager.getAliasConfig();
      if(_aliasConfig[aliasName] && _aliasConfig[aliasName][accountId])
      {
        let objConfig = _aliasConfig[aliasName][accountId];
            
        //Update client specific config
        config.auth.dlsIdentity.publicKey = objConfig.IDENTITY_PUBLIC_KEY;
        config.entitlements.publisherId = objConfig.PUBLISHER_ID;
        config.entitlements.apiUrl = objConfig.ENTITLEMENT_API_URL;
        config.email.mailerId =  objConfig.MAILER_ID;
        config.email.redirectUrl = objConfig.REDIRECT_URL;
        config.email.platformName = objConfig.PLATFORM_NAME;
        config.account.clientName = objConfig.CLIENT_NAME;

        //Update Assignment Table name
        let tableName = 'ASSIGNMENT_TABLE' + '_' + aliasName;
        config.assignment.dynamodb.assignment_table = process.env[tableName] ? process.env[tableName] : config.assignment.dynamodb.assignment_table;
      }
    }


}