'use strict';

/* Dependencies */
import { Request, Response, NextFunction } from 'express';

import * as authService from '../services/auth.server.service';
import * as passport from 'passport';
import { Logger } from '../logger';
import { UnAuthorizedAccessError, ForbiddenError } from '../error/error.model';
import config from '../config';
import { comproDLS } from '../services/dls-instance.service';

require('../passport/dls-strategy.server.service')(passport);

/* Logger Instance */
const logger: Logger = Logger.getInstance(module.filename);

/* Function used for verifying JWT Token */
export let isAuthenticated = async (req: any, res: Response, next: NextFunction): Promise<void> => {

    if (logger.isDebugEnabled()) logger.debug('Auth Middleware for checking if request is authenticated');

    if( req.query["apiKey"] == undefined) {
        if (logger.isDebugEnabled()) logger.debug('No request headers specified');
        return next(new UnAuthorizedAccessError());
    }
    try {
     
        let authToken;
        authToken = req.query['apiKey'];

        if(Object.keys(req.body).length !== 0) {
            let parsedBody = JSON.parse(req.body);
            req.body = parsedBody;
      } 

        if (logger.isDebugEnabled()) logger.debug('Verify JWT Token');
        let jwtTokenPayload = await authService.verifyJWTToken(authToken) as {userId: string, authToken: string, orgId: string};

        if (logger.isDebugEnabled()) logger.debug('Decrypt DLS Token');
        let decryptedToken = authService.decryptDLSToken(jwtTokenPayload.authToken);

        if (logger.isDebugEnabled()) logger.debug('Setting request object with userid, authToken, dlsAccessToken');
        
        req.userId = jwtTokenPayload.userId;
        req.orgId = jwtTokenPayload.orgId;
        //req.authToken = jwtTokenPayload.authToken;
        req.dlsAccessToken = decryptedToken;
      
     req.token = decryptedToken;
     req.authStrategy = config.auth.passport.tokenStrategy
     req.organization = jwtTokenPayload.orgId;
     
        let dlsAuthResponse = await passportAuthentication(req, res, next) as { user: object, token: object };

        next();

    } catch(err) {
        if (logger.isDebugEnabled()) logger.debug('Error received while jwt verification');
        next(err);
    }

};

export let validateIdentityToken = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {

        let identityToken;
        identityToken = req.query['apiKey'];

        //let parsedBody = JSON.parse(req.body);

        if(Object.keys(req.body).length !== 0) {
            let parsedBody = JSON.parse(req.body);
            req.body = parsedBody;
        }
        
        if (!identityToken) {
            if (logger.isDebugEnabled()) logger.debug('No request headers specified');
            throw new UnAuthorizedAccessError();
        }
        //validate identity token
        if (logger.isDebugEnabled()) logger.debug('Verifying identity token');
        await authService.verifyIdentityToken(identityToken).catch((err) => {
            throw new UnAuthorizedAccessError();
        });

        next();
    } catch(err) {
        next(err);
    }
};

let passportAuthentication = (req: Request, res: Response, next: NextFunction): Promise<object> => {
    return new Promise((resolve, reject) => {
        passport.authenticate([config.auth.passport.strategy, config.auth.passport.tokenStrategy], (err, response) => {
            if (logger.isDebugEnabled()) logger.debug('Executing callback function for passport.authenticate');
            if (err) reject(err);
            if (response) resolve(response);
        })(req, res, next);
    })
};

export let isPermitted = (roles = []) => {    
    if (typeof roles === 'string') {
        roles = [roles];
    }
    
    // return a middleware
    return async (req: any, res: Response, next: NextFunction): Promise<void> => {
        try {
            let permitted = false;
            let params = {
                orgid: req.orgId,  
                userid: req.userId
            }   
            
            let userDetail = await authService.getUserProfile(params);
            
            const keys = Object.keys(userDetail.roles)
            for (const key of keys) {
                if(roles.includes(key))
                {
                    permitted = true;
                    break;
                }
            }

            if(permitted)
                next();
            else
              res.status(401).json({ message: 'Unauthorized request' });
    
        } catch(err) {
            logger.error('Error received while jwt verification');
            next(err);
        }
    }
}

export enum UserRoles {
    Admin = "admin",
    Teacher = "teacher",
    Superadmin = "superadmin",
    Student = "student"
}

// 'use strict';

// /* Dependencies */
// import { Request, Response, NextFunction } from 'express';
// import { comproDLS } from '../services/dls-instance.service';

// import { Logger } from '../logger';
// import { BadRequestError, ForbiddenError, NotFoundError } from '../error/error.model';


// /* Logger Instance */
// const logger: Logger = Logger.getInstance(module.filename);


