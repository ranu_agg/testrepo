const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

//const AWS = require("aws-sdk");
import config from '../config';

console.log("config.aws.accessKeyId",config.aws.accessKeyId)
if(config.aws.accessKeyId != "") {
    AWS.config.update({
        region: 'us-east-2',
        accessKeyId: config.aws.accessKeyId,
        secretAccessKey: config.aws.secretAccessKey
    });
}
else{
    AWS.config.update({region: config.aws.region});
}

export { AWS } ;