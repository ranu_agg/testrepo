'use strict';
import { comproDLS } from '../services/dls-instance.service';
import config from '../config';
import { Logger } from '../logger';
import { UnAuthorizedAccessError, InternalServerError } from '../error/error.model';


const auth = comproDLS.Auth();
const logger: Logger = Logger.getInstance(module.filename);
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const jwkToPem = require('jwk-to-pem');





/*********************************
 * Public Function definitions
 **********************************/

/* Function used for creating JWT Token */
export let createJWTToken = async (dlsAuthResponse: { user: object, token: object }) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function createJWTToken');
    let encryptedToken: string;
    let user = dlsAuthResponse.user as {uuid: string, org: {id: string}};
    let userId: string = user.uuid;

    // Token received after successful DLS Authentication
    let dlsToken = JSON.stringify(dlsAuthResponse.token);

    // Encypt DLS Access Token
    encryptedToken = encryptDLSToken(dlsToken);

    // Payload object - data to be stored inside the JWT
    let payload = {
        authToken: encryptedToken,
        userId: userId,
        orgId: user.org.id
    } as {authToken: string, userId: string};

    let jwtAuthToken = await signJwtToken(payload) as string;
    let successResponse = {
        success: true,
        user: user,
        authToken: jwtAuthToken
    } as {success: boolean, user: object, authToken: string};

    return successResponse;
};

/* Function used for verifying JWT Token
   @param1 token - JWT Token
   @param2 callback - callback function to be executed after verifying token
 */
export let verifyJWTToken = (authToken: string) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function verifyToken');
    return new Promise((resolve, reject) => {

        jwt.verify(authToken, config.auth.jwtSecret , function(err, decode) {
            if (logger.isDebugEnabled()) logger.debug('Executing callback function for jwt.sign');
            if (err) {
                if (logger.isDebugEnabled()) logger.debug('Rejecting createJWTToken promise with error');
                if(err.name === 'TokenExpiredError') {
                    return reject(new UnAuthorizedAccessError('Session Expired', 'TOKEN_EXPIRED'));
                }
                return reject(new UnAuthorizedAccessError(err.message));
            } else {
                if (logger.isDebugEnabled()) logger.debug('Resolving createJWTToken promise with success response');
                resolve(decode);
            }
        });
    });

};

/*
    Accepts a valid JWT token and returns a new token with updated expiration time.
*/
export let refreshJwtToken = async (oldJwtToken) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function auth.server.service.refreshJwtToken');

    try {
        let payload: any = await verifyJWTToken(oldJwtToken);
        delete payload.exp;
        delete payload.iat;
        let jwtAuthToken = await signJwtToken(payload);    
        return jwtAuthToken;
    } catch(err) {
        throw err;
    }
}

export let verifyDLSToken = (orgId, dlsAccessToken: {access_token: string, refresh_token: string, expires_in: number}) => {
    return comproDLS.authWithToken(orgId, dlsAccessToken, {});
}

export let refreshDLSToken = async () => {
    let dlsAuthResponse = await comproDLS.refreshToken();
    return createJWTToken(dlsAuthResponse);
}

/* Accepts a payload and returns the signed JWT token */
let signJwtToken = (payload: any) => {
    return new Promise((resolve, reject) => {
        jwt.sign(payload, config.auth.jwtSecret, {expiresIn: config.auth.jwtExpiry}, function(err, jwtAuthToken){
            if (logger.isDebugEnabled()) logger.debug('Executing callback function for auth.server.service.signJwtToken');
            if (err) {
                if (logger.isDebugEnabled()) logger.debug('Rejecting auth.server.service.signJwtToken promise with error');
                reject(new UnAuthorizedAccessError());
            } else{
                if (logger.isDebugEnabled()) logger.debug('Resolving auth.server.service.signJwtToken promise with success response');
                resolve(jwtAuthToken);
            }
        });    
    });
}

/* Function used for decrypting DLS Token
  @param1 token - Encrypted DLS Token
  return decrypted DLS Token
 */
export let decryptDLSToken = (dlsToken: string) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function decryptDLSToken');
    let textParts = dlsToken.split(':');
    let iv = new Buffer(textParts.shift(), 'hex');
    let encryptedText = new Buffer(textParts.join(':'), 'hex');

    const hash = crypto.createHash("sha1");
    hash.update(config.auth.crypto.salt);

    let key = hash.digest().slice(0, 16);

    let decipher = crypto.createDecipheriv(config.auth.crypto.algorithm, key, iv);
    let decryptedToken = decipher.update(encryptedText);
    decryptedToken = Buffer.concat([decryptedToken, decipher.final()]).toString();
    if (logger.isDebugEnabled()) logger.debug('Returning decrypted token');
    return JSON.parse(decryptedToken);
};


/*********************************
 * Private Function definitions
 **********************************/

/* Function used for encrypting DLS Token
 @param1 token - DLS Access Token
 return encrypted DLS Token
 */
let encryptDLSToken = (dlsToken: string) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function encryptDLSToken');
    const iv = crypto.randomBytes(16);

    const hash = crypto.createHash("sha1");
    hash.update(config.auth.crypto.salt);

    let key = hash.digest().slice(0, 16);

    const cipher = crypto.createCipheriv(config.auth.crypto.algorithm, key, iv);
    let encrypted = cipher.update(dlsToken);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    if (logger.isDebugEnabled()) logger.debug('Returning encrypted token');
    return iv.toString('hex') + ':' + encrypted.toString('hex');
};

/* Function used for verifying Identity JWT Token
   @param1 token - JWT Token
   @param2 callback - callback function to be executed after verifying token
 */

export let verifyIdentityToken = (authToken: string) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function verifyToken');
    return new Promise((resolve, reject) => {
        let jwk;
        try {
            jwk = JSON.parse(Buffer.from(config.auth.dlsIdentity.publicKey, 'base64').toString());
        } catch(err) {
            return reject(new InternalServerError('Incorrect identity JWK key'));
        }
        jwt.verify(authToken, jwkToPem(jwk.keys[1]), { algorithms: ['RS256']}, function(err, decode) {
            if (logger.isDebugEnabled()) logger.debug('Executing callback function for jwt.sign');
            if (err) {
                if (logger.isDebugEnabled()) logger.debug('Rejecting createJWTToken promise with error');
                if(err.name === 'TokenExpiredError') {
                    return reject(new UnAuthorizedAccessError('Token Expired', 'TOKEN_EXPIRED'));
                }
                if(err.name === 'JsonWebTokenError') {
                    return reject(new UnAuthorizedAccessError('Invalid Token', 'INVALID_TOKEN'));
                }
                return reject(new UnAuthorizedAccessError(err.message));
            } else {
                if (logger.isDebugEnabled()) logger.debug('Resolving createJWTToken promise with success response');
                resolve(decode);                
            }
        });
    });

};

export let makeDlsProvision = (orgId:string,userObject: object) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function verifyToken');
        let dlsToken  = comproDLS.authWithAccountProvision(orgId, userObject, {});
        return dlsToken;
};

export let getExtUserToken = (orgId:string,userObject: {ext_user_id: string}) => {
    if (logger.isDebugEnabled()) logger.debug('Executing function get external user token command');
    return comproDLS.authWithExtUser(orgId, userObject).then(dlsToken => dlsToken);
};

export let getUserProfile = (params) => {
    let userDetail = auth.getUserProfile(params);
    return userDetail;
}

export let authWithToken = async (orgId, userToken) => {
    let response = await comproDLS.authWithToken(orgId, userToken, {});
    return response;

}   
