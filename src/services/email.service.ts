import { AWS } from "./aws-instance.service";

export class EmailService {

    private SES;
    constructor() {
        AWS.config.update({
            region: "us-east-1"
        });

        this.SES = new AWS.SES({
            apiVersion: '2010-12-01'
        })
    }

    /**
     * {
     *  templateName: string
     *  inviteeEmailId: string,
     *  templateData: string,
     *  inviterEmailId: string
     * }
     * @param inviteInfo : An object having info about user of mail
     */
    public sendInvite(inviteInfo) {
        ////Error handling for different scenario like template rendering failure and so is not implemented
        /// Since In future DLS invite email will be used then assuming they have handled such scenarios
        return new Promise((resolve, reject) => {
            try {

                let params = {
                    Template: inviteInfo.templateName,
                    Destination: {
                        ToAddresses: [
                            inviteInfo.inviteeEmailId
                        ]
                    },
                    Source: inviteInfo.inviterEmailId,
                    TemplateData: inviteInfo.templateData
                };


                this.SES.sendTemplatedEmail(params, (err, data) => {
                    if (err) {
                        console.log(err);
                        reject({
                            status: "failure",
                            userEmail: inviteInfo.inviteeEmailId
                        })
                    } else {
                        console.log("Success, Email Sent to " + inviteInfo.inviteeEmailId);
                        resolve({
                            status: "success",
                            userEmail: inviteInfo.inviteeEmailId
                        })
                    }
                });

            } catch (err) {
                console.log(err);
                //throw new InternalServerError(err.message); // To confirm what is the error message here
                reject({
                    status: "error",
                    userEmail: inviteInfo.inviteeEmailId
                });
            }
        });
    }
}