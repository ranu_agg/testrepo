import { comproDLS } from '../services/dls-instance.service';

const auth = comproDLS.Auth();


export class UsersService{

    public async getAllUsers(query){
        let users = auth.getAllUsers(query);
        return users;
    }
}