import { comproDLS } from '../services/dls-instance.service';
import config from '../config';

const spaces = comproDLS.Spaces(config.account.id);


export interface IGetUserSpacesOptions {
    extuserid: string;
    spaceRole?: string;
    spaceType?: string;
    spaceOrgContext?: string;
}

export class SpacesService {

    public async validateClassCode(query) {
        let userClass = spaces.validateClassCode(query);
        return userClass;
    }

    public async validateSpaceCode(query) {
        let result = spaces.validateSpaceCode(query);
        return result;
    }

    public async joinInstituteSpace(query){
        let result = spaces.joinInstituteSpace(query);
        return result;
    }
    
    public async provisionSpacesToStudent(query){
        let provisionResp = spaces.provisionSpacesToStudent(query);
        return provisionResp;
    }

    public async provisionSpacesToTeacher(query){
        let provisionResp = spaces.provisionSpacesToTeacher(query);
        return provisionResp;
    }

    public async getUserSpaces(options: IGetUserSpacesOptions) {
        
        return spaces.getUserSpaces(options);
    }
    public async entitleProductToUser(options){
        return spaces.entitleUserToProduct(options);
    }

    public async enrollUserInClassWithClassCode(options) {
        return spaces.enrollUserInClass(options);
    }
}