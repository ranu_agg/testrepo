import { AWS } from './aws-instance.service'
import { Logger } from '../logger';
let s3 = new AWS.S3();

export class S3InstanceService {
    static ListObjects = (bucketParams) => {
        return s3.listObjects(bucketParams).promise();
    }

    static GetObject = (bucketParams) => {
        return s3.getObject(bucketParams).promise()
    }
}
