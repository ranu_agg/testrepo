import { comproDLS } from '../services/dls-instance.service';
import config from '../config';

const product = comproDLS.Product(config.account.id);
const analytics = comproDLS.Analytics();


export class ProductService {

    public async entitleUserIntoProdut(userId, productId) {
        // var classEnrollmentOptions = {
        //     classid: classId,
        //     userid: userId
        // };
        // var auth = comproDLS.Auth();
        // let enrollPromise = auth.enrollUsertoClass(classEnrollmentOptions);

        let productEntitlementOptions = {
            productid: productId,
            userid: userId
        };
        return comproDLS.authWithCredentials("engage-dev", {username: "admin-engage-dev", password: "CoralDart57"}, {}).then(
            function success(response) {
                return product.entitleUsertoProduct(productEntitlementOptions); 
            },
            function error(err) {
                console.log(err)
            }
        );
        
    }
    public async getUserProducts(query) {
        let userProdusts = await product.getUserProducts(query);
        return userProdusts;
    }

    public async getUserProductAnalytics(params) {
        return analytics.getUserProductAnalytics(params);
    }
    
    public async getUserProductMeta(params) {
        return product.getProduct(params);
    }

    public getProductJsonWithResource(productJson) {
        const returnResource = (resourseId:string) => {
            return productJson.resources.generic[resourseId]
        }

        const insertResource = (item) => {
            if(item['item-type'] === 'folder' && item.items.length) {
                item.items = item.items.map((item) => {
                    return insertResource(item);
                });
            } else if(item['item-type'] === 'resource' && item['resource'] != null && item['resource'] != undefined) {
                let resourceNode = returnResource(item['resource'])
                item = {...item, ...resourceNode }
            }
            return item;
        }
        let items = productJson.items.default;
        items.map((item) => item = insertResource(item));

        return items;
    }
}