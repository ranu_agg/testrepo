const axios=require('axios')

export const resolveRequest:Function=(config:any)=>{
    try{
        const response = axios.request(config)
        return response
    }
    catch(error){
        console.log(error)
        return error;
    }
}