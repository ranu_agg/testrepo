import config from '../config';
const dlsEnv: string = config.dlsSdk.sdkEnvironment;
const comproDLS = require('comprodls-sdk').init(dlsEnv);
export { comproDLS };