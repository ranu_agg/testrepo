import { comproDLS } from '../services/dls-instance.service';

const product = comproDLS.Product();
const analytics = comproDLS.Analytics();
const xapi = comproDLS.Xapi();

export class XapirunnerService {
    public saveItemState(userId, productId, itemCode, state) {
        let options = {
            userid: userId,
            productid: productId,
            itemcode: itemCode,
            appdata: state
        }
        return analytics.updateAppState(options);
    }
    public getItemState(userId, productId, itemCode) {
        let options = {
            userid: userId,
            productid: productId,
            itemcode: itemCode
        }
        return analytics.getAppState(options);
    }
    public postStatement(newStatement) {
        let options = newStatement[0];
        return xapi.postStatement(options);
    }
}