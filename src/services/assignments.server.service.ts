import {IAssignmentProvider} from './assignments/IAssignmentProvider';
import {AssignmentProviderFactory} from './assignments/AssignmentProviderFactory'
import config from '../config';


const assignmentProvider: IAssignmentProvider = <IAssignmentProvider>AssignmentProviderFactory.GetObject(config.assignment.dataStore);

export class AssignmentsService {
  public getClassAssignments(params) {
    let res =  assignmentProvider.getAllAssignedPathsOfClass(params);
    return res;
  }

  public async addClassAssignment(params) {
    let res = assignmentProvider.createAssignedPath(params);
    return res;
  }

  public async deleteClassAssignment(params) {
    let res = assignmentProvider.deleteAssignedPath(params);
    return res;
  }

  public async updateClassAssignment(params) {
    let res = assignmentProvider.updateAssignedPath(params);
    return res;
  }
}
