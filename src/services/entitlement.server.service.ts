
import { resolveRequest } from "./http-service";
import { BadRequestError, InternalServerError } from '../error/error.model';

import config from '../config';

export class EntitlementService {

    public async redeemCode(accessCode,xAuth) {
        
        let publisherId = config.entitlements['publisherId'];
        let apiUrl = config.entitlements['apiUrl'];

        let axiosConfig = {
            method: 'patch',
            url: apiUrl+`/publisher/${publisherId}/access-code/redeem`,
            params: { 
                'access_code':accessCode
            },
            headers:{
                'X-Authorization': xAuth
            }
        }
        return resolveRequest(axiosConfig).then(succ=>succ.data,err=>{
            if(err.response.status === 410 || err.response.status === 404){
                throw new BadRequestError('Access Code redeem request failed !!!');
            }else{
                throw new InternalServerError("Access Code Redeem Api failed");
            }
        });
    }

    public async giveProductIdforLicense(redeemResponse){
        let productCodeArr = [];
         let parts = redeemResponse.data.package.digital_parts;
         for (let part of parts){
            let custAttribs = part.custom_attributes;
            custAttribs = custAttribs.filter( custAttrib => custAttrib.name === "product-code");
            if(custAttribs.length > 0){
                productCodeArr.push(custAttribs[0].value)
            }
         }
         return productCodeArr;
         
    }
    
}