import { comproDLS } from '../services/dls-instance.service';

const auth = comproDLS.Auth();

const analytics = comproDLS.Analytics();

export class ClassService {

    public async getUserClasses(query) {
        let userClass = auth.getUserClasses(query);
        return userClass;
    }

    public async enrollUserInClass(userId, classid) {

        let userClassEnrollResponse = auth.enrollUsertoClass({ userid: userId, classid: classid });
        return userClassEnrollResponse;
    }

    public async getClassDetails(query) {
        return auth.getParticularClass(query);
    }

    public async getClassUsers(query) {
        return auth.getClassUsers(query);
    }

    public async getClassRecordUserAggregations(query) {
        return analytics.getClassRecordUserAggregations(query);
    }

    public async unEnrollMultiUsersFromClass(query) {
        return auth.unEnrollMultiUserstoClass(query)
    }

    public async getStudentAnalytics(query) {
        let classDetailsParams = {
            classId: query.classId
        }
        const classUsersParms = {
            classid: query.classId,
            limit : 110
        }
        let [classDetails, classUsers] = await Promise.all([
            this.getClassDetails(classDetailsParams),
            this.getClassUsers(classUsersParms)
        ])
        let productArray = classDetails.products
        let productAnalyticsArray = await Promise.all(productArray.map((product, index) => {
            let queryParams = {
                classid: query.classId,
                productid: product.uuid
            }
            return this.getClassRecordUserAggregations(queryParams)
        }))

        let products =  await this.getProductsData(productArray, productAnalyticsArray, query.classId)
        let students = this.getStudentData(classUsers.entities, products, productAnalyticsArray);

        return { products, students , classId : query.classId }
    }


    private async getProductsData(productArray, productAnalytics, classId) {
        let productsData = [];
        let tempUserAnalyticsArray = await Promise.all(productAnalytics.map((product , index)=>{
            let userIds= Object.keys(product.users)
            if(userIds.length){
                let firstStudentId = userIds[0];
                let queryParams = {
                    productid : productArray[index].uuid,
                    userid : firstStudentId,
                    metrics : true,
                    type : "items",
                    classid : classId
                }
                    return this.getUserProductAnalytics(queryParams)
            }
            else{
                    return Promise.resolve({__analytics : { itemsCount : {testItems : 0 }}})
            }
        }));

        productArray.forEach((product, index) => {
            let testItems = tempUserAnalyticsArray[index]['__analytics'].itemsCount.testItems;
            productsData.push({
                productcode: product.meta.code,
                producttype: product.meta.producttype,
                items: productAnalytics[index].product.items,
                testItems : testItems
            })
        })

        return productsData;
    }


    private getStudentData(usersData, productArray, productsAnalytics) {
        let studentList = []
        usersData.forEach((user, index) => {
            if (user.roles.find((role, index) => role == "student")) {
                let studentData = {
                    meta: {
                        fn: user.first_name,
                        ln: user.last_name,
                        email: user.email,
                        uuid: user.uuid
                    }
                }
                productArray.forEach((product, index) => {
                    let userAnalytics = productsAnalytics[index].users[user.uuid];
                    if (userAnalytics) {
                        studentData[product.productcode] = userAnalytics.items
                    }
                })
                studentList.push(studentData)
            }
        })
        return studentList;
    }

    public async generateClassCode(query) {
        return auth.generateClassCode(query);
    }

    public async createClass(query){
        return auth.createClass(query);
    }

    public async updateClass(query){
        return auth.updateClass(query);
    }

    public async associateProduct(query) {
        return auth.createClassProductAssociation(query);
    }

    public async unassociateProduct(query) {
        return auth.removeClassProductAssociation(query);
    }

    public async getUserProductAnalytics(query){
        return analytics.getUserProductAnalytics(query);
    }

    public async updateClassSettings(query){
        return auth.updateClassSettings(query);
    }

    public async getActiveClasses(){
        return auth.getActiveClasses();
    }
}