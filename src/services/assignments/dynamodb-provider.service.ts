import { IAssignmentProvider } from './IAssignmentProvider';
import config from '../../config';
import { Logger } from '../../logger';
import { AWS } from "../aws-instance.service";
import { v1 as uuid } from 'uuid';

const logger: Logger = Logger.getInstance(module.filename);

AWS.config.update({region: config.aws.region});
console.log("config.assignment.dynamodb.assignment_table",config.assignment.dynamodb.assignment_table)
let docClient = new AWS.DynamoDB.DocumentClient();


export class DynamoDBProvider implements IAssignmentProvider {  
    public createAssignedPath = async (params) => {
      const assigned_path_id = uuid();
      params.assigned_path_id = assigned_path_id;
      
      let updatedParams = {
        TableName: config.assignment.dynamodb.assignment_table,
        Item: params
      };     

      let res = await docClient.put(updatedParams).promise();      
      return { success: true, assigned_path_id: assigned_path_id};     
    };

    public updateAssignedPath = async (params) => {
      let updatedParams = {
        TableName: config.assignment.dynamodb.assignment_table,
        Key: { assigned_path_id : params.assigned_path_id,
        classid: params.classid        
       },
       UpdateExpression: "set title = :t, startdate=:s, duedate=:d",
        ExpressionAttributeValues:{
            ":t":params.title,
            ":s":params.startdate,
            ":d":params.duedate
        }        
      };
      let res = await docClient.update(updatedParams).promise();
      return { success: true };
    };

    public deleteAssignedPath = async (params) => {
      let updatedParams = {
        TableName: config.assignment.dynamodb.assignment_table,
        Key: {
            assigned_path_id: params.assigned_path_id,
            classid: params.classid
        }
    }

      let res = await docClient.delete(updatedParams).promise();

    return { success: true };
    };

    public getAllAssignedPathsOfClass = async (params) => {

      let updatedParams = {
        TableName: config.assignment.dynamodb.assignment_table,
        FilterExpression: 'classid = :this_class',
        ExpressionAttributeValues: { ':this_class': params.classid },
      };

      function doScan(response) {
        if (response.error) 
        {
          if (logger.isDebugEnabled())
          logger.debug('Error');
        }
        
        else {
          // More data.  Keep calling scan.
          if ('LastEvaluatedKey' in response.data) {
            response.request.updatedParams.ExclusiveStartKey =
              response.data.LastEvaluatedKey;
            docClient.scan(response.request.updatedParams).on('complete', doScan).promise();
          }
        }
      }
  
      let {Items} = await docClient.scan(updatedParams).on('complete', doScan).promise();
  
      //convert Items into key value pair
      let res = {};
      Items.forEach(item => {
          res[item.assigned_path_id] = item;
      })
  
      return res;
    };

    public getMyAssignedPathsOfClass = (reqData) => {

    };

    
}
