
export interface IAssignmentProvider {
    createAssignedPath(params: any): any;
    updateAssignedPath(params: any): any;
    deleteAssignedPath(params: any): any;
    getAllAssignedPathsOfClass(params: any): any;
    getMyAssignedPathsOfClass(params: any): any;
}
