/* tslint:disable */
import { DynamoDBProvider } from './dynamodb-provider.service';
import { DLSProvider } from './dls-provider.service';

 export class AssignmentProviderFactory {

    static GetObject (name : string) : object {
        switch(name)
        {
            case 'dynamodb' : return new DynamoDBProvider();
            case 'comprodls' : return new DLSProvider();            
        }
    }

 }