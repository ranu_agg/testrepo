//import { bundle } from "../temp-assets/product.bundle";
import { AWS } from "./aws-instance.service";
import config from '../config';

let s3 = new AWS.S3();


export class BundleService {    
        public async getBundles() { 
        let bucketName;
        bucketName = config.aws.s3Bucket;
        let bucketParams = {Bucket: config.aws.s3Bucket, Key: config.account.id + '/bundles.json'};
        console.log("Bundle Bucket Params",bucketParams);
        let mydata = await s3.getObject(bucketParams).promise();
        return (JSON.parse(mydata.Body.toString()));
    }
}