'use strict';

const config = {
    port: process.env.PORT || 4400,
    auth: {
        jwtSecret: process.env.JWT_SECRET || 'MyS3cr3tK3Y',
        jwtExpiry: process.env.JWT_EXPIRATION_TIME || '6d',
        admin: {
            "username": "adminUser",
            "password": "adminPassword"
        },
        "passport": {
            strategy: "dls-login",
            tokenStrategy: "dls-token"
        },
        "crypto": {
            algorithm: 'aes-128-cbc',
            salt: 'S3cr3tK3Y'
        },
        dlsIdentity: {
            publicKey: process.env.IDENTITY_PUBLIC_KEY || "eyJrZXlzIjpbeyJhbGciOiJSUzI1NiIsImUiOiJBUUFCIiwia2lkIjoiekNOejBZSytyQkpwb2ZJQitkbHFzbS95STRSZXV0cG9uV1JENjR4SlRyTT0iLCJrdHkiOiJSU0EiLCJuIjoic21oZWpJd1BKT01ORGpPbXdiWmY4eFRJZ2pGN2Q3dVU3ZUU3Xy14YzFHRllnSzJvUlNxN2JrV2hhVXlvaHcydXRwNGNaQ0txYUVkelRNLVB2M3NoSVl5ek5mQUNhc2JnVy1uMDRvLWtRdExtUFJqRjlubUhhMk93aER5MlcteUpRT3YzaGJFT3JURjZla1BBRmxzdnN3MU9CZmpsU1I2YUpWTVdlalZKTG5BR2dIQzlxcmhTUnVsY28zQWNhcldCUHRDRVB3X1BJS1pOZlZGTmNQUDRyTWxiOWMwb2NlNTRTUU1uX1lYS3Q2YkZMXzdwZk1RM2kzU1pPVThNc0MwajJPMFpzXy1YZnJZeUNZR1RFRHVZZlRVc19EcTY1Ukk2eHhPLWNKbUJ2Qkl4XzNocTFDRko5YXJFcXJmMllESkdraEh5MTVzSGg0ZGpCbXg2eEJRa2F3IiwidXNlIjoic2lnIn0seyJhbGciOiJSUzI1NiIsImUiOiJBUUFCIiwia2lkIjoicXJ2ejFpWHpvZFdpUXBHUHBDV1pkUkd1VDhsWTBQcWJjcjltZEpRQk1wVT0iLCJrdHkiOiJSU0EiLCJuIjoicnZjU045SG1PN25MZ1NhREZtZUdKNlYtLXB3TXVQaUVEeWl0c3FtU2RpYWpWVWlHVVpQMXRocUJXOFl6SWIxa1o3MTltQzRIMHpnQ0RLam0yaXVoamVHUW9QcjJ6WVdJY2I0WktZd3Z6WTc3T1p4VVRYNnJmekxPMjdyVHBzV0VyUkNPNGNDeDE4WlpiVEo4N1h3Vmc4Q1YyVUJ1b25heHE1dFZwc2JPS2dFWU1wQ3NjWG5qaUhDdUdrMUF6dC1VOHN1WTI2ZjVCRE51NVBGWWZsQkNRRGFGNWlmS0tFS0kxMmxsZmtxV01ySjhYeHJXLUtRVVg5dTRvVGZiT0pQN0F0OTlVUjAyOWJ3SVpZdkRTT0M1RHM2dVR0RzY1M3BwUF9JUUliS1pWMHVxVjd4eXAzSFdXXzRMRXhLaTlhN1RoUHBmRWdmbTZ2LXJwS1NoZEd4dy13IiwidXNlIjoic2lnIn1dfQ=="
        }
    },
    dlsSdk: {
        "sdkEnvironment":  process.env.DLS_SDK_ENVIROMENT || "staging1",
        "admin": {
            "username": "adminUser",
            "password": "adminPassword"
        },
    },

   
    logger: {
        level: process.env.LOG_LEVEL || 'debug',
        transports: {
            active: process.env.LOG_TRANSPORTS || "file",
            console: {
                json: true,
                stringify: true,
                timestamp:true
            },
            file: {
                filename: (process.env.LOG_FILE_DIR || './logs') + '/engage.log',
                handleExceptions: true,
                humanReadableUnhandledException: true,
                datePattern: 'yyyy-MM-dd.',
                prepend: true
            }
        },
        request: {
            local_storage_key: "leonardo-request",
            id: "requestId",
        },
        exitOnError:false
    },
    account:{
        id:process.env.ACCOUNT_ID || "compro",
        clientName: process.env.CLIENT_NAME || "compro"
    },
    entitlements :{
        apiUrl:process.env.ENTITLEMENT_API_URL || 'https://dls-api-entitlement-qa.herokuapp.com',
        publisherId:process.env.PUBLISHER_ID || 'CDF3060E'
    },
    aws: {
        region: process.env.REGION || 'local',
        accessKeyId: '',
        secretAccessKey: '',
        s3Bucket: process.env.S3_BUCKET || 'engage-app-config1'
    },
    assignment: {
        dataStore: process.env.ASSIGNMENT_DATASTORE || "dynamodb",
        dynamodb:{
            assignment_table: process.env.ASSIGNMENT_TABLE || "Assignments"
        }
    },
    cors:{
        whiteListDomain:process.env.WHITE_LIST_DOMAIN ||"https://qa.engage.comprodls.com,https://sm.engage.comprodls.com,http://localhost:9000"
    },    
    email: {
        mailerId: process.env.MAILER_ID || 'qa.engage@yopmail.com',
        redirectUrl: process.env.REDIRECT_URL || 'https://qa.engage.comprodls.com/',
        platformName: process.env.PLATFORM_NAME || 'Engage'
    }
};

export default config;