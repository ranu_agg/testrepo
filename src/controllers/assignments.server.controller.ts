import { Request, Response, NextFunction, Router } from 'express';
import { AssignmentsService } from '../services/assignments.server.service';
import { Logger } from '../logger';
import config from '../config';
import { BadRequestError } from '../error/error.model';

const assignmentsService = new AssignmentsService();
const logger: Logger = Logger.getInstance(module.filename);

export let getClassAssignments = async (req: any, res: Response, next: NextFunction): Promise<void> => {
  try {
    if (logger.isDebugEnabled())
      logger.debug('Requesting list of class assignment');
    
    if (!req.params.classId) {
      throw new BadRequestError('Required params are missing');
    }

    const classid  = req.params.classId;

    let params = {
      classid:classid
    };

    if (logger.isDebugEnabled())
      logger.debug(
        'Executing Get Assignments with params: ' + JSON.stringify(params)
      );

    let assignments = await assignmentsService.getClassAssignments(params);
    const response = {
      classId: classid,
      list: assignments
    }
    res.json(response);
  } 
  catch (err) {
    logger.error(JSON.stringify(err));
    next(err);
  }
};

export let addClassAssignment = async (req: any, res: Response, next: NextFunction): Promise<void> => {
  try {
    if (logger.isDebugEnabled())
      logger.debug('Adding assignment in class');    

    if (!req.params.classId) {
      throw new BadRequestError('Required params are missing');
    }

    const classid  = req.params.classId;
    const {title,duedate,startdate,items} = req.body;    
    
    if(title && duedate && startdate && items && Array.isArray(items) && items.length > 0)
    {    
      let params = {
        assigned_path_id: '',
        classid: classid,
        title: title,      
        duedate: duedate,
        startdate: startdate,        
        items: items      
        };    

      if (logger.isDebugEnabled())
        logger.debug('Executing put Assignment query with params: ' + JSON.stringify(params));

      let response = await assignmentsService.addClassAssignment(params);
      res.status(201).json(response);
    }
    else{
      res.status(422).json({message: "Request data not in correct format"});
    }
  } 
  catch (err) {
    logger.error(JSON.stringify(err)); 
    next(err);
  }
};

export let deleteClassAssignment = async (req: any, res: Response, next: NextFunction): Promise<void> => {
    try{
        if (logger.isDebugEnabled())
            logger.debug('Deleting assignment from class');        

        if (!req.params.classId) {
          throw new BadRequestError('Required params are missing');
        }
          
        let params = {
          assigned_path_id: req.params.id,
          classid: req.params.classId
        }

        let assignment = await assignmentsService.deleteClassAssignment(params);
        res.json({ success: true });
    }
    catch(err){
        logger.error(JSON.stringify(err));
        next(err);
    }
}

export let updateClassAssignment = async (req: any, res: Response, next: NextFunction): Promise<void> => {
    try{
        if (logger.isDebugEnabled())
            logger.debug('Updating assignment from class');   

        if (!req.params.classId ) {
          throw new BadRequestError('Required params are missing');
        }

        const classid  = req.params.classId;  
        const {title,duedate,startdate} = req.body;    
        
        if(title && duedate && startdate)
        {    
          let params = {
            assigned_path_id: req.params.id,
            classid: classid,
            title: title,      
            duedate: duedate,
            startdate: startdate
            };               
      
          let assignment = await assignmentsService.updateClassAssignment(params);
          res.json({ success: true });        
        }
        else
        {
          res.status(422).json({message: "Request data not in correct format"});
        }
    }
    catch(err){
        logger.error(JSON.stringify(err));
        next(err);
    }
}

