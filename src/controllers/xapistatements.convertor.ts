/* Input - selectedProductCode, spaceId, classId, assignedPathId(optional) */
export class XAPIStatementConvertor {
    private selectedProductId;
    private spaceId;
    private classId;
    private userId;
    private assignedPathId;


    constructor(userId, productId, classId) {
        this.userId = userId;
        this.selectedProductId = productId;
        this.classId = classId;
    }

    //post statementObject to DLS 

    /** Function to update statement object to post to ComproDLS LRS.
     *  @param {Array} statementsArray
    */
    updateStatementObjectToPostToLRS(statementsArray) {
        let statementData = this.createStatementObject(statementsArray);
        statementsArray.forEach((statement) => {
            this.updateStatementEntities(statement, statementData);
        });
        return [statementData];
    }

    /** Function to create statement object to post to ComproDLS LRS.
     *  @param {Array} statementsArray
     *  @return {Object} statementObject
     */
    createStatementObject(statementsArray) {
        let statementObject = {
            'actor': {
                'uuid': this.userId//DLS userID
            },
            'product': {
                'uuid': this.selectedProductId
            },
            'platform': {
                'ua': ''
            },
            'entities': []
        };

        if (this.spaceId) {
            statementObject['space_key'] = this.spaceId;
        }

        if (this.classId) {
            statementObject['classid'] = this.classId;
        }

        if (statementsArray[0].json && statementsArray[0].json.context) {
            statementObject.platform.ua = statementsArray[0].json.context.platform.ua;
        }

        if (statementsArray[0].json && statementsArray[0].json['student_userid']) {
            statementObject['student_userid'] = statementsArray[0].json['student_userid'];
        }

        if (statementsArray[0].json && statementsArray[0].json.tags) {
            statementObject.product['tags'] = statementsArray[0].json.tags;
            statementObject.product['definition'] = statementsArray[0].json.definition;
        }

        /* For collab-group statements */
        if (statementsArray[0].json && statementsArray[0].json.group) {
            statementObject['group'] = statementsArray[0].json.group;
        }

        return statementObject;
    }

    /** Function to update statements object entities to post to ComproDLS LRS.
     *  @param {Object} statement
     *  @return {Object} statementObject
     */
    updateStatementEntities(statement, statementObject) {
        let entitiesJson = {}, statementJson = statement.json;
        if (this.assignedPathId) {
            entitiesJson['assigned_path_id'] = this.assignedPathId;
        }

        for (let key in statementJson) {
            if (key === 'verb') {
                let verbId = statementJson[key].id.split('/');
                verbId = verbId[verbId.length - 1];
                entitiesJson[key] = verbId;
            } else if (key === 'object') {
                entitiesJson['item-code'] = statementJson[key].id;
            } else if (key === 'timestamp') {
                entitiesJson[key] = this.convertTimestampToEpoc(statementJson[key]);
            } else if (key === 'result') {
                if (statementJson[key].duration) {
                    entitiesJson['timespent'] = this.convertTimespentToEpoc(statementJson[key].duration);
                } else {
                    entitiesJson[key] = statementJson[key];

                    // DLS api expects raw score as Integer. Max score and Raw are multiplied by 10 and then rounded to get the correct response.
                    if (statementJson[key].score) {
                        entitiesJson[key].score.raw = Math.round(entitiesJson[key].score.raw * 10);
                        entitiesJson[key].score.max = Math.round(entitiesJson[key].score.max * 10);
                    }
                }
            } else if (key === 'link-statementid') {
                entitiesJson[key] = statementJson[key];
            }
        }
        statementObject['entities'].push(entitiesJson);
    }

    /** Function to covert timestamp to epoc standard to post to comproDLS LRS.
     *  @param {Date} time format - 2018-03-06T16:10:15.553+05:30
     *  @return {Number} time in epoc msecs
     */
    convertTimestampToEpoc(time) {
        let date = new Date(time);
        return date.getTime();
    }

    /** Function to covert timespent to epoc standard to post to comproDLS LRS.
     *  @param {String} time format - PT35M48.90S
     *  @return {Number} time in epoc msecs
     */
    convertTimespentToEpoc(time) {
        let date = (time.split('PT'))[1], timespent = 0;
        if (date.indexOf('H') >= 0) {
            let hrs = (date.split('H'))[0];
            date = (date.split('H'))[1];
            timespent += hrs * 60 * 60;
        }
        if (date.indexOf('M') >= 0) {
            let mins = (date.split('M'))[0];
            date = (date.split('M'))[1];
            timespent += mins * 60;
        }
        if (date.indexOf('S') >= 0) {
            let secs = (date.split('S'))[0];
            timespent += secs * 1;
        }

        return timespent * 1000;
    }
}