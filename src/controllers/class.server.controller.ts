import { Request, Response, NextFunction, Router } from "express";
import { ClassService } from '../services/class.server.service';
import { EmailService } from '../services/email.service';
import * as authService from '../services/auth.server.service';
import { SpacesService } from "../services/spaces.server.service";
import { BadRequestError, InternalServerError, CustomEngageError, EngageErrors } from "../error/error.model";
import { EntitlementService } from '../services/entitlement.server.service';
import config from '../config';
import { ProductService } from '../services/product.server.service';


const classService = new ClassService();
const emailService = new EmailService();
const spacesService = new SpacesService();
const entitlementService = new EntitlementService();
const productService = new ProductService();

import { Logger } from '../logger';

const logger: Logger = Logger.getInstance(module.filename);

////Exprected query params
//settings=all     Get all classes //This is default
//setting=active   Get active classes
//setting=inactive Get active classes
export let getUserEnrolledClasses = async (req: any, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting user enrolled class from DLS');
        let settings = req.query.settings;

        let query = {
            userid: req.query.userId,
            details: true
        }

        let userClasses;

        if (logger.isDebugEnabled()) logger.debug('Executing user enrolled class query: ' + JSON.stringify(query));
        if (settings === 'active') {
            userClasses = await classService.getActiveClasses();
        }
        else if (settings === 'inactive') {
            userClasses = await classService.getUserClasses(query);
            let inactiveClassList = [];
            for (let i = 0; i < userClasses.entities.length; i++) {
                if (userClasses.entities[i]["class.settings.active"] === false) {
                    inactiveClassList.push(userClasses.entities[i])
                }
            }
            userClasses.entities = inactiveClassList;
            userClasses.count = inactiveClassList.length;
        }
        else {
            userClasses = await classService.getUserClasses(query);
        }

        userClasses.orgId = req.orgId;
        res.json(userClasses);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

/**
 * This API returns class details of a specific class using classId.
 * @param req 
 * @param res 
 * @param next 
 */
export let getClassDetails = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting Class Details from DLS');

        if (!req['userId'] || !req.params.classId) {
            throw new BadRequestError('Required params are missing');
        }

        const classDetailsParms = {
            classId: req.params.classId
        }
        const classMetadata = await classService.getClassDetails(classDetailsParms);
        res.json(classMetadata);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let getClassUserList = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting Class Details from DLS');

        if (!req['userId'] || !req.params.classId) {
            throw new BadRequestError('Required params are missing');
        }

        const classUsersParms = {
            classid: req.params.classId,
            limit: req.query.limit
        }
        const classUsers = await classService.getClassUsers(classUsersParms);
        const response = {
            classId: classUsersParms.classid,
            list: classUsers
        }
        res.json(response);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

export let generateClassCode = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting Class Details from DLS');

        if (!req['userId'] || !req.params.classId) {
            throw new BadRequestError('Required params are missing');
        }

        const classParams = {
            classid: req.params.classId
        }
        const classUsers = await classService.generateClassCode(classParams);
        res.json(classUsers);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

export let createClass = async (req: any, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Creating a new class from DLS');

        if (!req['userId']) {
            throw new BadRequestError('Required params are missing');
        }

        const classDetail = await classService.createClass(req.body);
        res.json(classDetail);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

export let updateClass = async (req: any, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Update a new class from DLS');

        if (!req['userId'] || !req.params.classId) {
            throw new BadRequestError('Required params are missing');
        }

        let classDetail:any = {};
        let params = req.body;
        params.classid = req.params.classId;

        //Activate the class
        if(req.body.settings)
        {
            let query = {
                classid: req.params.classId,
                settings: {active: true}
            }
            const response = await classService.updateClassSettings(query)
            classDetail.settings = response;
        }

        //Update class
        classDetail = await classService.updateClass(params);  
       
        if (req.body.unassociateProducts) {
            const productAssociateResponse = await updateProductClassAssociation(params.classid, req.body.unassociateProducts, ClassProductAssociation.Unassociate)
            classDetail.products = productAssociateResponse;
        }

        if (req.body.products) {
            const productAssociateResponse = await updateProductClassAssociation(params.classid, req.body.products, ClassProductAssociation.Associate)
            classDetail.products = productAssociateResponse;
        }

        if (!req.body.class_code) {
            const classParams = {
                classid: params.classid
            }
            const response = await classService.generateClassCode(classParams);
            classDetail.class_code = response.class_code;
        }

        res.json(classDetail);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

export let associateProduct = async (req: any, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Creating a new class from DLS');

        if (!req['userId'] || !req.params.classId || !req.body) {

            throw new BadRequestError('Required params are missing');
        }

        const response = await updateProductClassAssociation(req.params.classId, req.body, ClassProductAssociation.Associate)

        res.json(response);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

export let unassociateProduct = async (req: any, res: Response, next: NextFunction): Promise<void> => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Creating a new class from DLS');

        if (!req['userId'] || !req.params.classId || !req.body) {

            throw new BadRequestError('Required params are missing');
        }

        const response = await updateProductClassAssociation(req.params.classId, req.body, ClassProductAssociation.Unassociate)

        res.json(response);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

let updateProductClassAssociation = async (classId: string, productArray: string[], association: string) => {
    let classParams: any = {
        classId: classId
    }
    const promises = productArray.map(async product => {
        classParams.productId = product;
        console.log("classParams", classParams)
        let status;
        if (association === ClassProductAssociation.Associate)
            status = await classService.associateProduct(classParams);
        else
            status = await classService.unassociateProduct(classParams);
        console.log("status", status);
        return status
    })

    return await Promise.all(promises);
}

enum ClassProductAssociation {
    Associate = "associate",
    Unassociate = "unassociate"
}

export let getStudentsAnalytics = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting analytics for users in a class');
        if (!req.params.classId) {
            throw new BadRequestError("Required params missing");
        }
        let queryParams = {
            classId: req.params.classId
        };

        let analytics = await classService.getStudentAnalytics(queryParams)
        res.json(analytics)
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

/**
 * This function sends the invite email to the users
 */
export let addMultiUsersToClass = async (req: any, res: Response, next: NextFunction) => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting Email Invites');
        let inviterName = req.body.instructorName;
        let inviterEmail = req.body.instructorEmail;
        let className = req.body.className;
        let classKey = req.body.classKey;
        let platformURL = config.email.redirectUrl;
        let clientName = config.account.clientName;
        let platformName = config.email.platformName;
        let accessCode = req.body.accessCode;
        let xAuth = req.body.xAuthorisation;
        let productCodeArr;
        let productCode4provision;
        if (accessCode) {
            let redeemResp;
            try {
                if (logger.isDebugEnabled()) logger.debug('Redeeming Access Code');
                redeemResp = await entitlementService.redeemCode(accessCode, xAuth);
                if (logger.isDebugEnabled()) logger.debug('Access Code Redeemed');
            } catch (err) {
                throw new CustomEngageError(EngageErrors.INVALID_ACCESS_CODE)
            }

            productCodeArr = await entitlementService.giveProductIdforLicense(redeemResp);
            if(productCodeArr.length === 0){
                throw new CustomEngageError(EngageErrors.NO_PRODUCT_ASSOCIATED);
            }
            productCode4provision = productCodeArr[0];
        }
         
        if (req.body.user) {
            let inviteeData = req.body.user;
            let username = inviteeData.email.replace('@', '_');
            let isExistingUser = false;
            if (inviteeData.ext_account_status == "new") {
                if(productCode4provision) {
                    productCodeArr = productCodeArr.slice(1);
                    let provisionParams = {
                        "ext_user_id": username,
                        "ext_role": inviteeData.role,
                        "class_code": classKey,
                        "product_code": productCode4provision,
                        "ext_first_name":inviteeData.firstName ,
                        "ext_last_name": inviteeData.lastName,
                        "ext_email": inviteeData.email,
                        "ref_id": username
                    }
                    //Provisioning spaces to the new user
                    try {
                        if (logger.isDebugEnabled()) logger.debug('Provisioning spaces to student');
                        let response = await spacesService.provisionSpacesToStudent(provisionParams);
                    } 
                    catch (err) {
                        throw new CustomEngageError(EngageErrors.ACCOUNT_PROVISION_FAILED);
                    }    
                }
            }
            else {
                isExistingUser = true;
                let params = {
                    "ext_user_id": username,
                    "ext_first_name": inviteeData.firstName,
                    "ext_last_name": inviteeData.lastName,
                    "ext_email": inviteeData.email,
                    "ref_id": username,
                    "ext_role": inviteeData.role,
                    "class_code": classKey
                    }
                try {
                    let response = await spacesService.enrollUserInClassWithClassCode(params);
                }
                catch (err) {
                    throw new CustomEngageError(EngageErrors.CLASS_ENROLLMENT_FALIED);
                }
            }
            // Doing product entitlement if Access Code is provided
            if (productCodeArr && productCodeArr.length > 0) {
                let userProduct = [];
                if(isExistingUser){
                    let userObj = {
                        ext_user_id: username,
                    };
                    const dlsAuthResponse = await authService.getExtUserToken(req.params.orgId, userObj);
                    let userId = dlsAuthResponse.user.uuid;

                    let query = {
                        userid: userId,
                        details:true
                    }
                    if (logger.isDebugEnabled()) logger.debug('Executing user products query: ' + JSON.stringify(query));
                    userProduct = await productService.getUserProducts(query);
                }

                for (let productCode of productCodeArr) {
                    let isAlreadyEntitled = userProduct.find(product => product.meta.code == productCode);
                    if (!isAlreadyEntitled){
                        let entitleProductParams = {
                            "ext_user_id": username,
                            "ext_role": inviteeData.role,
                            "product_code": productCode
                        }
                        try {
                            let response = await spacesService.entitleProductToUser(entitleProductParams);
                        }
                        catch (err) {
                            throw new CustomEngageError(EngageErrors.PROD_ENTITLEMENT_FALIED);
                        }
                    }
                }
            }

            //Sending emails to every user
            let inviteeName = inviteeData.lastName ? `${inviteeData.firstName} ${inviteeData.lastName}` : `${inviteeData.firstName}`;
            let mailTemplateParams = {
                inviteeName: inviteeName,
                inviterName: inviterName,
                inviterEmail: inviterEmail,
                className: className,
                classKey: classKey,
                classUrl: platformURL,
                platformName: platformName
            }
            let templateType = (isExistingUser || accessCode) ? "welcome"  : "invitation";
            let templateName = `${clientName}-${inviteeData.role}-${templateType}`;
            let mailInfo = {
                templateName: templateName,
                inviteeEmailId: inviteeData.email,
                templateData: JSON.stringify(mailTemplateParams),
                inviterEmailId: config.email.mailerId
            }
            
            try {
                await emailService.sendInvite(mailInfo);
                res.send({
                    "success": inviteeData.email
                });
            }
            catch {
                throw new CustomEngageError(EngageErrors.EMAIL_SENDING_FALIED);
            }
        }
        else {
            throw new BadRequestError('Required params are missing');
        }
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

export let unEnrollMultiUsersFromClass = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting to unenroll list of users from a class');
        if (!req.params.classId || !req.body.users) {
            throw new BadRequestError("Required params missing");
        }
        let query = {
            classid: req.params.classId,
            users: req.body.users
        }
        const users = await classService.unEnrollMultiUsersFromClass(query)
        res.json(users)
    } catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}

export let updateClassSettings = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting to update class state to active/inactive');
        if (!req.params.classId || !req.body) {
            throw new BadRequestError("Required params missing");
        }
        let query = {
            classid: req.params.classId,
            settings: req.body
        }
        const response = await classService.updateClassSettings(query)
        res.json(response)
    } catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
}