import { Request, Response, NextFunction, Router } from "express";
import { SpacesService } from "../services/spaces.server.service";
import { Logger } from '../logger';
import { BadRequestError, InternalServerError } from "../error/error.model";

const spacesService = new SpacesService();

const logger: Logger = Logger.getInstance(module.filename);

export let validateClassCode = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Validating Class Code from DLS');
        let query = {
            classCodeBody : {
                "class_code": req.query.classCode
            }}
        if (logger.isDebugEnabled()) logger.debug('Executing validate class code query: ' + JSON.stringify(query));
        let userClasses = await spacesService.validateClassCode(query)
      
        res.json(userClasses);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let validateSpaceCode = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Validating Space Code from DLS');
        let query = {
            spaceCodeBody : {
                "space_code": req.query.spaceCode
            }}
        if (logger.isDebugEnabled()) logger.debug('Executing validate space code query: ' + JSON.stringify(query));
        let result = await spacesService.validateSpaceCode(query)
      
        res.json(result);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let getUserSpaces = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Get user spaces from DLS');
        const email = req.query.email as string;
       
        if (!email) {
            throw new BadRequestError('Required params are missing');
        }

        if (logger.isDebugEnabled()) logger.debug('Fetching user spaces');
        const userId = email.replace('@', '_');
        const userSpaces = await spacesService.getUserSpaces({extuserid: userId}).catch((err) => {
            throw new InternalServerError('DLS getUserSpaces failed');
        });
        
        res.json(userSpaces);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};


