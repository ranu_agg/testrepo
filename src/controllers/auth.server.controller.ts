import { Request, Response, NextFunction, Router } from "express";

import { BadRequestError, InternalServerError } from '../error/error.model';
import * as authService from '../services/auth.server.service';



import { Logger } from '../logger';

const logger: Logger = Logger.getInstance(module.filename);

export let getDlsTokenExtUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Validating DLS token');
        let email = req.body.email;
        let orgId = req.params.orgId;      
        
        if (!(email && orgId)) {
            throw new BadRequestError('Required params are missing');
        }

        let userId = email.replace('@', '_');
        let userObj = {
            ext_user_id: userId,
        };
        const dlsAuthResponse = await authService.getExtUserToken(orgId, userObj).catch((err) => {
            throw new InternalServerError('DLS getExtUserToken failed');
        });

        if (logger.isDebugEnabled()) logger.debug('Creating JWT token');
        const jwtToken  =  await authService.createJWTToken(dlsAuthResponse);
        res.json(jwtToken);     
    }catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};



