// import { Request, Response, NextFunction, Router } from "express";

// import { BadRequestError, InternalServerError, UnAuthorizedAccessError, NotFoundError } from '../error/error.model';
// import { Logger } from '../logger';
// import * as config from '../config';

// import * as passport from 'passport';
// import * as authService from '../services/authentication/auth.server.service';
// import * as gatewayService from '../services/authentication/gateway.server.service';

// const logger: Logger = Logger.getInstance(module.filename);

// export let ssoAccess = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
//     try {
//         if (logger.isDebugEnabled()) logger.debug("Get request to launch paint from external");

//         let token = req.query.accessToken;
//         let orgId = req.query.orgId;
//         let mode = req.query.mode || 'default';
//         let launchView = req.query.launchView;
//         // Organization based authStrategies mapping can be there
//         let authStrategy = req.query.authStrategy || config.auth.passport.tokenStrategy;

//         if (!(token && orgId && launchView)) {
//             throw new BadRequestError('Required params are missing');
//         }

//        req.body.token = token;
//        req.body.organization = orgId;
//        req.body.mode = mode;
//        req.body.authStrategy = authStrategy;

//         let dlsAuthResponse = await passportAuthentication(req, res, next) as { user: object, token: object };
//         let paintResponse:any = await authService.createJWTToken(dlsAuthResponse) as {success: boolean, user: object, authToken: string};
//         let paintContext = gatewayService.getPaintContext(paintResponse);
//         paintContext.externalConfig.launchView = launchView;
//         paintContext.externalConfig.mode = mode;
//         res.cookie("paintContext", JSON.stringify(paintContext));
//         res.redirect('/#/externalConfig/redirect');

//     } catch (err) {
//         next(err);
//     }
    
// }
// let passportAuthentication = (req: Request, res: Response, next: NextFunction): Promise<object> => {
//     return new Promise((resolve, reject) => {
//         passport.authenticate([req.body.authStrategy], (err, response) => {
//             if (logger.isDebugEnabled()) logger.debug('Executing callback function for passport.authenticate');
//             if (err) reject(err);
//             if (response) resolve(response);
//         })(req, res, next);
//     })
// };