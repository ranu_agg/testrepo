import { Request, Response, NextFunction, Router } from "express";
import { comproDLS } from '../services/dls-instance.service';
import { BundleService } from '../services/bundle.server.service';

const bundleService = new BundleService();

import { Logger } from '../logger';
import { BadRequestError } from "../error/error.model";

const logger: Logger = Logger.getInstance(module.filename);

export let getBundles = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting user enrolled class from DLS');        
        
        let prodBundles = await bundleService.getBundles();        
       
        res.json(prodBundles);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};