import { Request, Response, NextFunction, Router } from "express";

import { BadRequestError, ForbiddenError, NotFoundError } from '../error/error.model';
import { ProductService } from '../services/product.server.service';
import { ClassService } from '../services/class.server.service';

const productService = new ProductService();
const classService = new ClassService();

import { Logger } from '../logger';

const logger: Logger = Logger.getInstance(module.filename);

import { XapirunnerService } from "../services/xapirunner.server.service";

export let getUserProducts = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting user products from DLS');
        let query = {
            userid:req.query.userId,
            details:true
        }
        if (logger.isDebugEnabled()) logger.debug('Executing user products query: ' + JSON.stringify(query));
        let userProduct = await productService.getUserProducts(query);
        res.json(userProduct);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let getUserProductDetails = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting user product details from DLS');

    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let getProductWithAnalytics = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting TOC data from DLS');
        let role = req.query.role;

        if (!req['userId'] || !req.params.productId) {
            throw new BadRequestError('Required params are missing');
        }
        let tocparams = {
            type: 'items',
            userid: req['userId'],
            productid: req.params.productId
        }
        let metaparams = {
            productid: tocparams.productid,
            details: false
        }

        let response;
        if(role === 'teacher')
        {
            metaparams.details = true;
            let productData = await productService.getUserProductMeta(metaparams);
            let updatedItems = productService.getProductJsonWithResource(productData);
            response = {
                meta: productData.meta,
                result: updatedItems,
            } 
        }
        else
        {
            const tocResponse = await productService.getUserProductAnalytics(tocparams);
            const metaResponse = await productService.getUserProductMeta(metaparams);
            response = {
                meta: metaResponse.meta,
                result: tocResponse.result,
                __analytics:tocResponse.__analytics
            }            
        }
        res.json(response);
                
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let getItemsWithAnalytics = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting TOC data from DLS');

        if (!req['userId'] || !req.params.productId) {
            throw new BadRequestError('Required params are missing');
        }

        let params: {[k: string]: any} = {
            type: 'items',
            userid: req['userId'],
            productid: req.params.productId
        }
        
        //Read item code if available
        let itemCode = req.query.itemcode;
        if(itemCode)
        {
            params.itemcode = itemCode;
        }
        const response = await productService.getUserProductAnalytics(params);
        
        res.json(response);
                
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};