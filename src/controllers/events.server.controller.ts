import { Request, Response, NextFunction, Router } from "express";
import { comproDLS } from '../services/dls-instance.service';
import config from '../config';

var push = comproDLS.PushX();

import { Logger } from '../logger';

const logger: Logger = Logger.getInstance(module.filename);

export let getGrantKeysByOrgId =  async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        let ext_user_id = req.body.ext_user_id;        
        ext_user_id = ext_user_id.replace('@', '_');
        
        let orgid = req.params.orgId;
        let role = req.body.role;

        var options = { ext_user_id: ext_user_id, ext_role:role };

        if (logger.isDebugEnabled()) logger.debug('Requesting grand keys');

        let authAccount = await comproDLS.authWithAccountProvision(orgid, options, {})        
        let data = await push.grantByUserOrgId({})
        if (logger.isDebugEnabled()) logger.debug('Grant keys received');
            data.userid=authAccount.user.uuid
        res.send(data)
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let getGrantKeysByAccountId =  async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        let userid = req.body.userid;
        if (logger.isDebugEnabled()) logger.debug('Requesting grand keys');
        let data = await push.grantByAccountId({
            'accountId': config.account.id,
            'refId': userid,
        })
        data['env'] = config.dlsSdk.sdkEnvironment;
        if (logger.isDebugEnabled()) logger.debug('Grant keys received');
        res.send(data)
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};


