import { Request, Response, NextFunction, Router } from "express";
import { EntitlementService } from '../services/entitlement.server.service';
import { SpacesService } from '../services/spaces.server.service';
import { BadRequestError, InternalServerError } from '../error/error.model';
import { Logger } from '../logger';

const entitlementService = new EntitlementService();
const spacesService = new SpacesService();
const logger: Logger = Logger.getInstance(module.filename);

export let studentRegistration = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        let email = req.body.email;
        let firstName = req.body.firstName;
        let lastName = req.body.lastName;
        let role = req.body.role;
        let classCode = req.body.classCode;
        
        if(role !== "student"){
            throw new BadRequestError("Role should be student")
        }

        if (!(email && firstName && role && classCode)) {
            throw new BadRequestError('Required params are missing');
        }
        let username = email.replace('@', '_');
        let orgId;
        
        //validate access token
        
        let accessCode = req.body.accessCode;
        let xAuth = req.body.xAuthorisation;

        if (logger.isDebugEnabled()) logger.debug('Redeeming Access Code');
        let redeemResp = await entitlementService.redeemCode(accessCode,xAuth);
        if (logger.isDebugEnabled()) logger.debug('Access Code Redeemed');
        
        let productCodeArr = await entitlementService.giveProductIdforLicense(redeemResp);
        if(productCodeArr.length === 0){
            throw new BadRequestError('Access Code do not have any linked products')
        }

        let productCode4provision = productCodeArr[0];
        //Start dls provision
        let provisionParams = {
            "ext_user_id": username,
            "ext_role": role,
            "class_code": classCode,
            "product_code": productCode4provision,
            "ext_first_name":firstName,
            "ext_last_name":" ",
            "ext_email": email,
            "ref_id": username
          }
        if(lastName!==undefined) provisionParams["ext_last_name"]=lastName;
        if (logger.isDebugEnabled()) logger.debug('Provisioning spaces to student');
        let provisionResp = await spacesService.provisionSpacesToStudent(provisionParams);
         for(let resp of provisionResp.entities) {
             if(resp.space_type === "institutional")
             orgId = resp.dls_org_id;
         }          

        //removing already provisioned product from array
        productCodeArr = productCodeArr.slice(1);
        
        for(let productCode of productCodeArr){
        let entitleProductParams = {
                "ext_user_id": username,
                "ext_role": role,
                "product_code": productCode,
                "ref_id": username
            }
        let prodEntResp = await spacesService.entitleProductToUser(entitleProductParams);
        }
    
        res.json(provisionResp);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};

export let teacherRegistration = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        let email = req.body.email;
        let firstName = req.body.firstName;
        let lastName = req.body.lastName;
        let role = req.body.role;
        let spaceCode = req.body.spaceCode;
        let accessCode = req.body.accessCode;
        if(role !== "teacher"){
            throw new BadRequestError("Role should be teacher")
        }

        if (!(email && firstName && role && (spaceCode || accessCode))) {
            throw new BadRequestError('Required params are missing');
        }
        let username = email.replace('@', '_');
        let orgId;
        if(spaceCode){
            let query = {
                spaceCodeBody : {
                    "space_code": spaceCode
                }}
            if (logger.isDebugEnabled()) logger.debug('Executing validate space code query: ' + JSON.stringify(query));
            let validateSpaceCodeResult = await spacesService.validateSpaceCode(query).catch((err)=>{
                if(err.response.status === 404){
                    throw new BadRequestError('Invalid Space Code');
                }else{
                    throw new InternalServerError("Validate Space Code Api failed");
                }
            })
    
            //Join institution space
            let joinInstParams = {
                "ext_user_id": username,
                "ext_role": role,
                "space_code": spaceCode,
                "ext_first_name":firstName,
                "ext_last_name":" ",
                "ext_email": email
              }
            if(lastName!==undefined) joinInstParams["ext_last_name"]=lastName;
            if (logger.isDebugEnabled()) logger.debug('Provisioning spaces to student');
            let joinInstResponse = await spacesService.joinInstituteSpace(joinInstParams);
            
            res.json({validateSpaceCodeResult,joinInstResponse});

        }else{
            let xAuth = req.body.xAuthorisation;
            if (!(accessCode && xAuth)) {
                throw new BadRequestError('Required params are missing');
            }
            if (logger.isDebugEnabled()) logger.debug('Redeeming Access Code');
            let redeemResp = await entitlementService.redeemCode(accessCode,xAuth);
            if (logger.isDebugEnabled()) logger.debug('Access Code Redeemed');
            
            let productCodeArr = await entitlementService.giveProductIdforLicense(redeemResp);
            if(productCodeArr.length === 0){
                throw new BadRequestError('Access Code do not have any linked products')
            }
    
            //Start dls provision
            let provisionParams = {
                "ext_user_id": username,
                "ext_role": role,
                "ext_first_name":firstName,
                "ext_last_name":" ",
                "ext_email": email,
                "ref_id": username
              }
            if(lastName!==undefined) provisionParams["ext_last_name"]=lastName;
            if (logger.isDebugEnabled()) logger.debug('Provisioning spaces to student');
            let provisionResp = await spacesService.provisionSpacesToTeacher(provisionParams);
            for(let resp of provisionResp.entities) {
            //  if(resp.space_type === "institutional")
                orgId = resp.dls_org_id;
            }          
            
            for(let productCode of productCodeArr){
                let entitleProductParams = {
                        "ext_user_id": username,
                        "ext_role": role,
                        "product_code": productCode
                    }
                let prodEntResp = await spacesService.entitleProductToUser(entitleProductParams);
                res.json(provisionResp);
            }
        
        }
        
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};
