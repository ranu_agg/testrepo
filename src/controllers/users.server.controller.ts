import { Request, Response, NextFunction, Router } from "express";
import { UsersService } from '../services/users.server.service'
import { Logger } from '../logger';
import { BadRequestError } from "../error/error.model";

const usersService = new UsersService();
const logger: Logger = Logger.getInstance(module.filename);
const lookupMap = {
    "email": "ext_email",
    "name": "name"
}

export let getAllUsers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Requesting user all User in organisation');

        if (!req.params.searchString) {
            throw new BadRequestError('Required params are missing');
        }
        let lookupIndex = req.query.lookup as string;
        let lookup;
        req.query.lookup ? lookup = `${lookupMap[lookupIndex] + ":" + req.params.searchString + "*"}` : null

        let query = {
            search: req.query.searchString,
            limit: req.query.limit,
            lookup: lookup,
            role: req.query.role
        }
        let users = await usersService.getAllUsers(query);

        res.json(users);
    }
    catch (err) {
        if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
        next(err);
    }
};