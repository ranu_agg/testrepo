import { Request, Response, NextFunction, Router } from "express";

import { BadRequestError, ForbiddenError, NotFoundError, InternalServerError } from '../error/error.model';
import { XapirunnerService } from "../services/xapirunner.server.service";

import { Logger } from '../logger';

const logger: Logger = Logger.getInstance(module.filename);

import config from '../config';
import { XAPIStatementConvertor } from "./xapistatements.convertor";

export let saveItemState = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Saving player item state');
        const productId = req.params.productId;
        const itemCode = decodeURIComponent(req.params.itemCode);
        const userId = req['userId'];
        const state = req.body;

        if(!userId || !productId || !itemCode) {
            throw new BadRequestError('Required params are missing');
        }

        let xapiRunnerService = new XapirunnerService();
        xapiRunnerService.saveItemState(userId, productId, itemCode, state).then(() => {
            res.status(200).send();
        }).catch((err) => {
            if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
            throw err;
        });
    }
    catch (err) {
        next(err);
    }
};

export let getItemState = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        //Stubbing - hardcoded product id for demo
        if (logger.isDebugEnabled()) logger.debug('Requesting player item state');

        const productId = req.params.productId;
        const itemCode = decodeURIComponent(req.params.itemCode);
        const userId = req['userId'];
        
        if(!userId || !productId || !itemCode) {
            throw new BadRequestError('Required params are missing');
        }

        let xapiRunnerService = new XapirunnerService();
        xapiRunnerService.getItemState(userId, productId, itemCode).then((itemState) => {
            res.status(200).json(itemState);
        }).catch((err) => {
            if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
            throw err;
        });
    }
    catch (err) {
        next(err);
    }
};

export let postStatement = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {
        if (logger.isDebugEnabled()) logger.debug('Posting statement');
        const productId = req.body.productId;
        const userId = req['userId'];
        const newStatement = req.body.statement;
        const classId = req.body.classId;

        if(!userId || !productId ) {
            throw new BadRequestError('Required params are missing');
        }

        let xapiRunnerService = new XapirunnerService();
        let xapiStatementConvertor = new XAPIStatementConvertor(userId,productId,classId);
        let statementData = xapiStatementConvertor.updateStatementObjectToPostToLRS(newStatement);

        xapiRunnerService.postStatement(statementData).then((response) => {
            res.status(200).send(response);
        }).catch((err) => {
            if (logger.isDebugEnabled()) logger.error(JSON.stringify(err));
            throw err;
        });
    }
    catch (err) {
        next(err);
    }
};