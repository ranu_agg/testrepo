import { AWS } from './aws-instance.service';

AWS.config.update({
    accessKeyId: "<AWS access key>",
    secretAccessKey: "<AWS secret access key>",
    region: "us-east-1"
});

let GrupoSM_Student_Invite = `
<div style="width: 100%;background: rgb(199, 195, 195) !important;box-sizing: border-box;">
    <div style="max-width: 600px; margin: auto;padding: 20px 0;">
        <div style="width: 100%;background: white;padding: 15px;box-sizing: border-box;">
            <div>
                <img src="https://engage-pub1.s3.us-east-2.amazonaws.com/email-assets/engage-logo.png" style="max-width: 100%; max-height: 300px; display: block; margin: auto" alt="comproDLS-engage">
            </div>
            <div style="font-family: sans-serif;">
                <h4>Hi {{inviteeName}},</h4>
                <p>Welcome to the <b>{{platformName}}</b> family. <b>{{inviterName}}</b> ({{inviterEmail}}) has invited you to join class <b>{{className}}</b>.</p>
                <p>Use the class key (<b>{{classKey}}</b>) to enter the class.</p>
                <p>To experience the next generation learning, click on the button below or copy and paste the link into your browser.</p>
                <div style="margin: 20px 0;">
                    <button style="color: #fff;
                    background-color: #e85181;
                    box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 5px -1px, rgba(0, 0, 0, 0.14) 0px 5px 8px 0px, rgba(0, 0, 0, 0.12) 0px 1px 14px 0px;
                    height: 50px;
                    width: 135px;
                    padding: 6px 16px;
                    font-weight: 600;
                    border-radius: 30px;
                    font-size: 16px;
                    display: block;
                    margin: auto;
                    border: #e85181; outline: none"><a href="{{classUrl}}"  target="_blank" style="text-decoration:none; color: inherit;">Get Started</a></button>
                </div>
                <a href="{{classUrl}}" target="_blank" >{{classUrl}}</a>
                <p style="margin-top: 30px">Cheers,<br />The Engage Team</p></div>
        </div>
    </div>
</div>`;

let GrupoSM_Student_Welcome = `
<div style="width: 100%;background: rgb(199, 195, 195) !important;box-sizing: border-box;">
    <div style="max-width: 600px; margin: auto;padding: 20px 0;">
        <div style="width: 100%;background: white;padding: 15px;box-sizing: border-box;">
            <div>
                <img src="https://engage-pub1.s3.us-east-2.amazonaws.com/email-assets/engage-logo.png" style="max-width: 100%; max-height: 300px; display: block; margin: auto" alt="comproDLS-engage">
            </div>
            <div style="font-family: sans-serif;">
                <h4>Hi {{inviteeName}},</h4>
                <p>Welcome to the <b>{{platformName}}</b> family. You have been specifically added in class <b>{{className}}</b> by <b>{{inviterName}}</b> ({{inviterEmail}}).</p>
                <p style="margin-top: 30px">To experience the next generation learning, click on the button below or copy and paste the link into your browser.</p>
                <div style="margin: 20px 0;">
                    <button style="color: #fff;
                    background-color: #e85181;
                    box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 5px -1px, rgba(0, 0, 0, 0.14) 0px 5px 8px 0px, rgba(0, 0, 0, 0.12) 0px 1px 14px 0px;
                    height: 50px;
                    width: 135px;
                    padding: 6px 16px;
                    font-weight: 600;
                    border-radius: 30px;
                    font-size: 16px;
                    display: block;
                    margin: auto;
                    border: #e85181; outline: none"><a href="{{classUrl}}"  target="_blank" style="text-decoration:none; color: inherit;">Get Started</a></button>
                </div>
                <a href="{{classUrl}}" target="_blank" >{{classUrl}}</a>
                <p style="margin-top: 30px">Cheers,<br />The Engage Team</p>
            </div>
        </div>
    </div>
</div>`;

let GrupoSM_teacher = `
<div style="width: 100%;background: rgb(199, 195, 195) !important;box-sizing: border-box;">
    <div style="max-width: 600px; margin: auto;padding: 20px 0;">
        <div style="width: 100%;background: white;padding: 15px;box-sizing: border-box;">
            <div>
                <img src="https://engage-pub1.s3.us-east-2.amazonaws.com/email-assets/engage-logo.png" style="max-width: 100%; max-height: 300px; display: block; margin: auto" alt="comproDLS-engage">
            </div>
            <div style="font-family: sans-serif;">
                <h4>Hi {{inviteeName}},</h4>
                <p>{{inviterName}} ({{inviterEmail}}) has invited you to teach in class <b>{{className}}</b>.</p>
                <p>Your class key is : <b>{{classKey}}</b></p>
                <p>To accept the invitation click the button below or copy and paste the link into your browser.</p>
                <div style="margin: 20px 0;">
                    <button style="color: #fff;
                    background-color: #e85181;
                    box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 5px -1px, rgba(0, 0, 0, 0.14) 0px 5px 8px 0px, rgba(0, 0, 0, 0.12) 0px 1px 14px 0px;
                    height: 50px;
                    width: 135px;
                    padding: 6px 16px;
                    font-weight: 600;
                    border-radius: 30px;
                    font-size: 16px;
                    display: block;
                    margin: auto;
                    border: #e85181; outline: none"><a href="{{classUrl}}"  target="_blank" style="text-decoration:none; color: inherit;">Join Class</a></button>
                </div>
                <a href="{{classUrl}}" target="_blank" >{{classUrl}}</a>
                <p>If you think someone invited you by mistake, feel free to ignore this email.</p>
            </div>
            <div style="margin-top: 30px;font-family: sans-serif;">
                <hr>
                <p style="margin-bottom: 0;"><i>Team Engage</i></p>
            </div>
        </div>
    </div>
</div>
`

let Compro_student_welcome = `<div style="width: 100%;background: rgb(199, 195, 195) !important;box-sizing: border-box;">
<div style="max-width: 600px; margin: auto;padding: 20px 0;">
    <div style="width: 100%;background: white;padding: 15px;box-sizing: border-box;">
        <div>
            <img src="https://engage-pub1.s3.us-east-2.amazonaws.com/email-assets/engage-logo.png" style="max-width: 100%; max-height: 300px; display: block; margin: auto" alt="comproDLS-engage">
        </div>
        <div style="font-family: sans-serif;">
            <h4>Hi {{inviteeName}},</h4>
            <p>Welcome to the <b>{{platformName}}!</b>.</p>
            <p>You have been invited to participate in the course <b>{{className}}</b> by your instructor <b>{{inviterName}}</b> ({{inviterEmail}}).</p>
            <div style="margin: 20px 0;">
                <button style="color: #fff;
                background-color: #e85181;
                box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 5px -1px, rgba(0, 0, 0, 0.14) 0px 5px 8px 0px, rgba(0, 0, 0, 0.12) 0px 1px 14px 0px;
                height: 50px;
                width: auto;
                padding: 6px 16px;
                font-weight: 600;
                border-radius: 30px;
                font-size: 16px;
                display: block;
                margin: auto;
                border: #e85181; outline: none"><a href="{{classUrl}}"  target="_blank" style="text-decoration:none; color: inherit;">Launch {{platformName}}</a></button>
            </div>
            <p>You may need to register with <b>{{platformName}}</b> before you can access the course.</p>
            <p>If you have trouble accessing the link above, try copying and pasting the following link into your browser: <a href="{{classUrl}}" target="_blank" >{{classUrl}}</a></p>
            <p style="margin-top: 30px">Cheers,<br />The Engage Team</p></div>
    </div>
</div>
</div>`

let Compro_student_invitation = `<div style="width: 100%;background: rgb(199, 195, 195) !important;box-sizing: border-box;">
<div style="max-width: 600px; margin: auto;padding: 20px 0;">
    <div style="width: 100%;background: white;padding: 15px;box-sizing: border-box;">
        <div>
            <img src="https://engage-pub1.s3.us-east-2.amazonaws.com/email-assets/engage-logo.png" style="max-width: 100%; max-height: 300px; display: block; margin: auto" alt="comproDLS-engage">
        </div>
        <div style="font-family: sans-serif;">
            <h4>Hi {{inviteeName}},</h4>
            <p>Welcome to the <b>{{platformName}}!</b>.</p>
            <p>You have been invited to participate in the course <b>{{className}}</b> by your instructor <b>{{inviterName}}</b> ({{inviterEmail}}).</p>
            <div style="margin: 20px 0;">
                <button style="color: #fff;
                background-color: #e85181;
                box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 5px -1px, rgba(0, 0, 0, 0.14) 0px 5px 8px 0px, rgba(0, 0, 0, 0.12) 0px 1px 14px 0px;
                height: 50px;
                width: auto;
                padding: 6px 16px;
                font-weight: 600;
                border-radius: 30px;
                font-size: 16px;
                display: block;
                margin: auto;
                border: #e85181; outline: none"><a href="{{classUrl}}"  target="_blank" style="text-decoration:none; color: inherit;">Launch {{platformName}}</a></button>
            </div>
            <p>You would need to register with <b>{{platformName}}</b> before you can access the course. At the time of registration, please use the following course key: <b>{{classKey}}</b></p>
            <p>If you have trouble accessing the link above, try copying and pasting the following link into your browser: <a href="{{classUrl}}" target="_blank" >{{classUrl}}</a></p>
            <p style="margin-top: 30px">Cheers,<br />The Engage Team</p></div>
    </div>
</div>
</div>`
//Params to create a template for student
var params = {
    Template: { 
      TemplateName: "compro-student-invitation", 
      HtmlPart: Compro_student_invitation,
      SubjectPart: "Welcome to {{platformName}}!"      
    }
  }

  let SES =  new AWS.SES({
    apiVersion: '2010-12-01'
})

//**************Code below to create a template in AWS SES */
SES.createTemplate(params, (err, data) => {
    if (err) {
      console.log(err)
    }else{
        console.log("success");
    }    
  })