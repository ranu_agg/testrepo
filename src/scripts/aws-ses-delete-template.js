import { AWS } from './aws-instance.service';

AWS.config.update({
    accessKeyId: "<AWS access key>",
    secretAccessKey: "<AWS secret access key>",
    region: "us-east-1"
});

  let SES =  new AWS.SES({
    apiVersion: '2010-12-01'
})

//**************the Code below to delete a template in AWS SES */
  var templatePromise = SES.deleteTemplate({TemplateName: '<template name>'}).promise();

// Handle promise's fulfilled/rejected states
templatePromise.then(
  function(data) {
    console.log("Template Deleted");
  }).catch(
    function(err) {
    console.error(err, err.stack);
  });