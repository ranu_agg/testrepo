const router = require('express').Router();
import * as xapirunnerController from '../controllers/xapirunner.server.controller';

export default function () {
    // Define the product api releated routes api routes


    /**
     * @typedef GetItemStateRes
     * @property {ItemStateKey.model} key - Key Object
     * @property {object} token - Token Object
     * @property {string} authStrategy - Auth strategy
     * @property {string} organization - Organization
     * @property {object} server - Server Object
    */

    /**
     * @typedef ItemStateKey
     * @property {string} type - type
     * @property {string} itemId - Item ID
     * @property {string} status - Status
     * @property {object} itemBody - Item Body Object
     * @property {object} score - Score Object
    */

    /**
     * @typedef SaveItemStateReq
     * @property {ItemStateKey.model} key - Key Object
    */


    /**
     * @route POST /api/user/product/progress/state/{productId}/{itemCode}
     * @summary Saves Item state
     * @param {string} productId.path.required - Product ID
     * @param {string} itemCode.path.required - Item Code
     * @param {SaveItemStateReq.model} item.body.required - Item Body
     * @group XAPI Runner
     * @security JWT
    */
    router.post('/api/user/product/progress/state/:productId/:itemCode', xapirunnerController.saveItemState);

    /**
     * @route GET /api/user/product/progress/state/{productId}/{itemCode}
     * @summary Gets Item state
     * @param {string} productId.path.required - Product ID
     * @param {string} itemCode.path.required - Item Code
     * @group XAPI Runner
     * @returns {GetItemStateRes.model} 200 - Item state object
     * @security JWT
    */
    router.get('/api/user/product/progress/state/:productId/:itemCode', xapirunnerController.getItemState);
    router.post('/api/xapi-statements', xapirunnerController.postStatement);
    
    return router;

};