const router = require('express').Router();
import * as bundleController from '../controllers/bundle.server.controller';

export default function () {
    // Define the class enrollment releated api routes    
    router.get('/', bundleController.getBundles);
    return router;
};