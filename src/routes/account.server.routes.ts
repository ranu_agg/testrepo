import { Router } from "express";
const router = Router();
import registrationRoutes from './app.registration.routes';
import * as authMiddleware from '../middlewares/auth.middleware';
import spacesServerRoutes from "./spaces.server.routes";
import bundlesServerRoutes from "./bundle.server.routes";

import * as eventsController from '../controllers/events.server.controller';

export default function () {
  router.use('/bundles', authMiddleware.isAuthenticated, bundlesServerRoutes());   
  router.use('/spaces', authMiddleware.validateIdentityToken, spacesServerRoutes());
  router.post('/events/subscribe',authMiddleware.validateIdentityToken, eventsController.getGrantKeysByAccountId)
  router.use('/', registrationRoutes()); 
  return router;
};