import { Router } from "express";
const router = Router();
import xapirunnerRoutes from './xapirunner.server.routes'
import * as authMiddleware from '../middlewares/auth.middleware';
import accountRoutes from "./account.server.routes";
import orgRoutes from "./org.server.routes"

export default function () {

  router.use('/account', accountRoutes());
  router.use('/org/:orgId', orgRoutes());
  router.use('/lrs', authMiddleware.isAuthenticated,xapirunnerRoutes()); 

  return router;
};