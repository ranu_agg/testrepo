const router = require('express').Router({mergeParams: true});
import * as authController from '../controllers/auth.server.controller';

export default function () {
    // Define the product api releated routes api routes


    /**
     * @typedef DLSTokenReq
     * @property {string} email - Email ID of User
     * @property {string} orgId - Organization id
    */

    /**
     * @typedef UserResObj
     * @property {string} name - Name of user
     * @property {string} first_name - First Name of user
     * @property {string} last_name - Last Name of user
     * @property {string} uuid - Unique UID
     * @property {object} roles - User Role
     * @property {string} email - Email of user
     * @property {object} org - Organization of user
    */

    /**
     * @typedef DLSTokenRes
     * @property {boolean} success - Success Boolean
     * @property {UserResObj.model} user - User Object
     * @property {string} authToken - Auth Token
    */


    /**
     * @route POST /auth/dls-auth
     * @summary Get DLS ExtUser token
     * @group Authentication
     * @param {DLSTokenReq.model} user.body - User Account Provision Request Object
     * @returns {DLSTokenRes.model} 200 - User Account Provision Response object
     * @security JWT
    */
    router.post('/dls-auth', authController.getDlsTokenExtUser);

    return router;

};