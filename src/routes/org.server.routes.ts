import { Router } from "express";
const router = Router({mergeParams: true});
import classRoutes from './class.server.routes';
import authRoutes from './auth.server.routes';
import * as authMiddleware from '../middlewares/auth.middleware';
import usersRoutes from './users.server.routes';
import productRoutes from './product.server.routes';
import * as eventsController from '../controllers/events.server.controller';

export default function () {
  
  router.use('/auth', authMiddleware.validateIdentityToken, authRoutes());
  
//   router.use('/xapi-analytics', authMiddleware.isAuthenticated,xapirunnerRoutes()); 
//   router.use('/product', authMiddleware.isAuthenticated,productRoutes());
  router.use('/classes', authMiddleware.isAuthenticated,classRoutes());
   
  //Users
  router.use('/users', authMiddleware.isAuthenticated, usersRoutes());

  // events
  router.post('/events/subscribe', authMiddleware.isAuthenticated, eventsController.getGrantKeysByOrgId);
 
  router.use('/product', authMiddleware.isAuthenticated,productRoutes());

  
  
  return router;
};