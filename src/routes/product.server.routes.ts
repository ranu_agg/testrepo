const router = require('express').Router();
import * as productController from '../controllers/product.server.controller';

export default function () {
    // Define the product api releated routes api routes

    /**
     * @typedef TOCMeta
     * @property {string} schema - Schema Version
     * @property {string} thumbnaillarge - thumbnail
     * @property {string} author - Author
     * @property {object} paths - path object
     * @property {string} thumbnailsmall - thumbnail
     * @property {integer} pipelineversion - Pipeline Version
     * @property {string} title - Title
     * @property {integer} version - Version
     * @property {string} status - status
     * @property {string} registrationtitle - registration title
     * @property {string} code - code
     * @property {string} supertype - supertype
     * @property {string} producttype - producttype
     * @property {string} group - group
    */

    /**
     * @typedef Item
     * @property {string} item-type - Item type
     * @property {string} name - name
     * @property {string} sub-type - Sub type
     * @property {Array<Item>} items - Items Object
     * @property {string} item-code - Item code
     * @property {object} instructor - instructor
     * @property {object} __analytics - Analytics
    */

    /**
     * @typedef TOCResponse
     * @property {TOCMeta.model} meta - TOC meta
     * @property {Array<Item>} result - Item array
    */

    router.get('/user-products', productController.getUserProducts);

    router.get('/user-product-details', productController.getUserProductDetails);

    /**
     * @route GET /product/toc
     * @summary Gets TOC
     * @group Products
     * @returns {TOCResponse.model} 200 - TOC object
     * @security JWT
    */
    router.get('/:productId', productController.getProductWithAnalytics);

    router.get('/:productId/progress/items', productController.getItemsWithAnalytics);
    
    return router;

};