const router = require('express').Router({mergeParams: true});
import * as classController from '../controllers/class.server.controller';
import assignmentsRoutes from './assignments.server.routes';
import { NextFunction } from 'express';

export default function () {
    // Define the class enrollment releated api routes    
    router.get('/user-enrolled-classes', classController.getUserEnrolledClasses);  
    router.get('/:classId', classController.getClassDetails);
    router.get('/:classId/users', classController.getClassUserList);
    router.put('/:classId/settings', classController.updateClassSettings);

    //Class Assignment
    router.use('/:classId/assignments', assignmentsRoutes());
    
    //Class Creation
    router.post('/',classController.createClass);
    router.put('/:classId',classController.updateClass);
    router.post('/:classId/generate-class-code', classController.generateClassCode);
    router.post('/:classId/associate-product', classController.associateProduct);
    router.post('/:classId/unassociate-product', classController.unassociateProduct);

    //Class Management Routes
    router.post('/:classId/users/remove' , classController.unEnrollMultiUsersFromClass);
    router.post('/:classId/users/add' , classController.addMultiUsersToClass);
    router.get('/:classId/students/analytics' , classController.getStudentsAnalytics)
    
    return router;
};