const router = require('express').Router();
import * as registrationController from '../controllers/registration.server.controller';
import * as authMiddleware from '../middlewares/auth.middleware';

export default function () {
    // Define the class enrollment releated api routes
    router.post('/student/first-time-registration',authMiddleware.validateIdentityToken, registrationController.studentRegistration);
    router.post('/teacher/first-time-registration',authMiddleware.validateIdentityToken, registrationController.teacherRegistration);
    return router;

};