const router = require("express").Router({mergeParams: true});
import * as assignmentsController from "../controllers/assignments.server.controller";
import * as authMiddleware from '../middlewares/auth.middleware';

export default function () {
  // Define the class enrollment releated api routes
  let userRole = authMiddleware.UserRoles;
 // let permittedUserList = [userRole.Admin,userRole.Superadmin,userRole.Teacher];
  router.get("/", assignmentsController.getClassAssignments);
  router.post("/",assignmentsController.addClassAssignment);
  router.put('/:id', assignmentsController.updateClassAssignment);
  router.delete("/:id", assignmentsController.deleteClassAssignment);
  return router;
}