const router = require('express').Router({mergeParams: true});
import * as usersController from '../controllers/users.server.controller';

export default function () {
    router.get('/:searchString', usersController.getAllUsers)
    return router;
}