const router = require('express').Router();
import * as spacesController from '../controllers/spaces.server.controller';
import * as authMiddleware from '../middlewares/auth.middleware';

export default function () {
    // Define the class enrollment releated api routes
    router.get('/get-user-spaces', spacesController.getUserSpaces);
    router.post('/validate-class-code', spacesController.validateClassCode);
    router.post('/validate-space-code', spacesController.validateSpaceCode);
    return router;

};