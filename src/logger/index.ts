'use strict';

import * as winston from 'winston';
import 'winston-daily-rotate-file';
import * as fileSystem from 'fs';
import  config from '../config';
import * as mkdirp from 'mkdirp';
import * as pathModule from 'path';
import { getNamespace } from 'continuation-local-storage';

const logger = initializeWinstonLogger();

export class Logger {

    public static getInstance(module): Logger {
        return new Logger(module);
    }

    private moduleName: string;
    private namespace: any;
    private level: string;

    private constructor(moduleName: string) {
        this.moduleName = moduleName;
        this.namespace = getNamespace(config.logger.request.local_storage_key);
        this.level = config.logger.level;
    }

    public error(message) {
        this.log('error', message);
    }

    public warn(message) {
        this.log('warn', message);
    }

    public info(message) {
        this.log('info', message);
    }

    public debug(message) {
        this.log('debug', message);
    }

    public silly(message) {
        this.log('silly', message);
    }

    public isDebugEnabled() {
        return this.isLevelEnabled('debug');
    }

    public isWarnEnabled() {
        return this.isLevelEnabled('warn');
    }

    public isInfoEnabled() {
        return this.isLevelEnabled('info');
    }

    public isSillyEnabled() {
        return this.isLevelEnabled('silly');
    }

    private isLevelEnabled(logLevel:string) {
        return (winston.level[this.level] <= winston.level[logLevel]) ? true : true;//bug
    }

    private log(level, message) {

        let module = this.moduleName;
        try {

            let correlationID = config.logger.request.id;
            // if (this.namespace.get(config.logger.request.id)) {
            //     correlationID = this.namespace.get(config.logger.request.id);
            // }

            logger.log(
                level,
                message,
                {
                    module,
                    correlationID
                });

        } catch (err) {
            console.error('Unable to log error! - ', err);
        }
    }
}

function initializeWinstonLogger() {
    try {
        let transports = createWinstonTransports();
        return new winston.Logger({
            transports,
            exitOnError:  config.logger.exitOnError
        });
    } catch (err) {
        console.error('Error during initialization of logger', err);
        throw err;
    }
}

function createWinstonTransports() {
    let transports = [];
    let arrTransports = config.logger.transports.active.split(' ');
    for (let transport of arrTransports) {
        switch (transport) {
            case 'console':
                config.logger.transports.console.level = config.logger.level;
                transports.push(new winston.transports.Console(config.logger.transports.console));
                break;
            case 'file':
                let logDir: string = pathModule.dirname(config.logger.transports.file.filename);
                if (!fileSystem.existsSync(logDir)) {
                    createFolder(logDir);
                }
                config.logger.transports.file.level = config.logger.level;
                transports.push(new winston.transports.DailyRotateFile(config.logger.transports.file));
                break;
            default:
            // not supported transport
        }
    }
    return transports;
}

function createFolder(foldername: string) {
    mkdirp(foldername, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log(`The folder: ${foldername} was created`);
        }
    });
}