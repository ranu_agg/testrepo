FROM node:12.6.0-alpine
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
    
WORKDIR /server
COPY package.json /server
RUN npm install
COPY . /server
CMD node dist/index.js
EXPOSE 4400
