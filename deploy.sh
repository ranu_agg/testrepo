#! /bin/bash

npm install -g serverless
npm install webpack serverless-webpack serverless-offline serverless-plugin-tracing
serverless deploy --stage $env --package $CODEBUILD_SRC_DIR/target/$env -v -r us-east-1